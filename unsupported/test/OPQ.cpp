﻿// Copyright (C) 2014 Yannick Dueren, Julian Koellermeier, Roman Schaerer
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include "main.h"
#include <unsupported/Eigen/OPQ>
//#include <boost/multiprecision/cpp_bin_float.hpp>


using std::sqrt;
using std::pow;
using namespace Eigen;

template<typename Realtype>
class Polynomial1 
{
  public:
  Realtype eval(const Realtype& x) {
    return pow(x,9)-4.0*pow(x,5)+0.5*pow(x,4)-3.0*x*x + 14.0*x+22.0;
  }
};

template<typename Realtype>
class Polynomial2 
{
  public:
  Realtype eval(const Realtype& x) {
    return pow(x,6)+2*pow(x,5) - 8.0*pow(x,4)-2.0*x*x +12.0*x -8.0;
  }
};

template<typename Realtype>
class ExponentialFunction 
{
  public:
  Realtype eval(const Realtype& x) {
    return std::exp(x);
  }
};

template<typename Realtype>
class NegativeExponentialFunction{
  public:
  Realtype eval(const Realtype& x){
    return exp(-x);
  }
};

template<typename Realtype>
class ExpHermiteFunction
{
public:
  Realtype eval(const Realtype& x){
    return 2*x*x*exp(-x*x);
  }
};

template <typename Realtype>
class wfLegendre : public WeightFunction<Realtype>
{
public:
  wfLegendre() {this->a=-1.0; this->b=1.0;}
  //+(x-x) to prevent compiler warnings
  inline Realtype eval(Realtype x){return 1.0+(x-x);}
};


template<typename Realtype>
class wfChebyshev : public WeightFunction<Realtype>
{
public:
  wfChebyshev() {this->a=-1.0; this->b=1.0;}
  Realtype eval(Realtype x)
  {
    return sqrt(1.0-x*x);
  }
};

template <typename Realtype>
class wfLaguerre : public WeightFunction<Realtype>
{
public:
  wfLaguerre() {this->a = 0.0; this->b= 0.0;}
  Realtype eval(Realtype x){return std::exp(-x);}
};


template <typename Realtype>
class wfHermite : public WeightFunction<Realtype>
{
public:
  wfHermite() {this->a = 0.0; this->b= 0.0;}
  Realtype eval(Realtype x){return std::exp(-x*x);}
};

template<typename Realtype>
class wfChebyshevAlt : public WeightFunction<Realtype>
{
public:
  wfChebyshevAlt() {this->a=-1.0; this->b=1.0;}
  Realtype eval(Realtype x)
  {
    return 1+(x-x);
  }
};

template <typename Realtype>
class wfLaguerreAlt : public WeightFunction<Realtype>
{
public:
  wfLaguerreAlt() {this->a = 0.0; this->b= 0.0;}
  Realtype eval(Realtype x){return 1+(x-x);}
};

template <typename Realtype>
class wfHermiteAlt : public WeightFunction<Realtype>
{
public:
  wfHermiteAlt() {this->a = 0.0; this->b= 0.0;}
  Realtype eval(Realtype x){return 1+(x-x);}
};


template<typename Realtype, typename Arraytype>
class OPQTests
{
public:
  void TestCase(const int& N_Coeff,
                const int& N_iterations,
                typename OPQ<Realtype, Arraytype>::orthPolType polynomialType,
                typename OPQ<Realtype, Arraytype>::orthPolType quadraturetype,
                const Realtype& eps,
                int N_Nodes = -1,
                bool Scaling= true)
  {
    typedef OPQ<Realtype, Arraytype> opq;

    WeightFunction<Realtype>* wf = NULL;
    opq OrthPol;

    if(Scaling)
    {
      switch(polynomialType){
        case opq::LEGENDRE:
        default:
          wf = new wfLegendre<Realtype>;
          break;
        case opq::LAGUERRE:
          wf= new wfLaguerre<Realtype>;
          break;
        case opq::CHEBYSHEV:
          wf = new wfChebyshev<Realtype>;
          break;
        case opq::HERMITE:
          wf = new wfHermite<Realtype>;
          break;
      }
    } else
    {
      switch(polynomialType){
        case opq::LEGENDRE:
        default:
          wf = new wfLegendre<Realtype>;
          break;
        case opq::LAGUERRE:
          wf= new wfLaguerreAlt<Realtype>;
          break;
        case opq::CHEBYSHEV:
          wf = new wfChebyshevAlt<Realtype>;
          break;
        case opq::HERMITE:
          wf = new wfHermiteAlt<Realtype>;
          break;
      }
    }

    OrthPol.setQuadrature(quadraturetype, N_Nodes, Scaling);
    OrthPol.setWeightfunction(wf);
    Arraytype ab(2, 2*N_Coeff-1);
    ab = OrthPol.getExactCoefficients(polynomialType, 2*N_Coeff-1);

    OrthPol.computeChebyshev(ab, N_Coeff, N_iterations);

    Arraytype result(2, N_Coeff);
    result = OrthPol.coefficients();

    Matrix<Realtype, Dynamic, Dynamic> Error(2, N_Coeff);
    Error = result;
    Error -= ab.block(0,0,2, N_Coeff);
    for(int i = 0; i< N_Coeff; i++)
    {
      if(ab(0,1) != 0) Error(0,i) /= ab(0,i);
      if(ab(1,i) != 0) Error(1,i) /= ab(1,i);
    }
    Error = Error.cwiseAbs();
    std::cout << "Error: \n" << Error.block(0,0,2,N_Coeff) << "\n Norm: " << Error.block(0,0,2,N_Coeff).cwiseAbs().maxCoeff() << std::endl << std::endl;

    VERIFY(Error.block(0,0,2,N_Coeff).cwiseAbs().maxCoeff() < eps);

    delete wf;
  }

  void TestExactMoments(const int& N_Coeff,
                typename OPQ<Realtype, Arraytype>::orthPolType polynomialType,
                const Realtype& eps)
  {
    typedef OPQ<Realtype, Arraytype> opq;

    WeightFunction<Realtype>* wf = NULL;
    opq OrthPol;

    switch(polynomialType){
      case opq::LEGENDRE:
      default:
        wf = new wfLegendre<Realtype>;
        break;
      case opq::HERMITE:
        wf = new wfHermite<Realtype>;
        break;
    }
    OrthPol.setWeightfunction(wf);
    Arraytype ab(2, 2*N_Coeff-1);
    Arraytype Exact(2, 2*N_Coeff-1);
    Exact = OrthPol.getExactCoefficients(polynomialType, 2*N_Coeff-1);
    ab = Exact;

    //Moments
    Arraytype Moments(1,2*N_Coeff);
    switch(polynomialType)
    {
    case opq::LEGENDRE:
    default:
      Moments.setZero();
      Moments(0,0) = 2;
      break;
    case opq::HERMITE:
      Moments.setZero();
      Moments(0,0) = 1.772453850905516027298167483341145182797549456122;
      break;
    }

    OrthPol.computeChebyshevFromMoments(ab, Moments);

    Arraytype result(2, N_Coeff);
    result = OrthPol.coefficients();

    Matrix<Realtype, Dynamic, Dynamic> Error(2, N_Coeff);
    Error = result;
    Error.block(0,0,2, N_Coeff) -= Exact.block(0,0,2, N_Coeff);
    for(int i = 0; i< N_Coeff; i++)
    {
      if(Exact(0,1) != 0) Error(0,i) /= Exact(0,i);
      if(Exact(1,i) != 0) Error(1,i) /= Exact(1,i);
    }
    Error = Error.cwiseAbs();
    std::cout << "Error: \n" << Error.block(0,0,2,N_Coeff) << "\n Norm: " << Error.block(0,0,2,N_Coeff).cwiseAbs().maxCoeff() << std::endl << std::endl;

    VERIFY(Error.block(0,0,2,N_Coeff).cwiseAbs().maxCoeff() < eps);

    delete wf;
  }


  void TestCaseZeroCoeff(const int& N_Coeff,
                const int& N_iterations,
                typename OPQ<Realtype, Arraytype>::orthPolType polynomialType,
                typename OPQ<Realtype, Arraytype>::orthPolType quadraturetype,
                const Realtype& eps,
                int N_Nodes = -1,
                bool Scaling= true)
  {
    typedef OPQ<Realtype, Arraytype> opq;

    WeightFunction<Realtype>* wf = NULL;
    OPQ<Realtype, Arraytype> OrthPol;

    if(Scaling)
    {
      switch(polynomialType){
        case opq::LEGENDRE:
        default:
          wf = new wfLegendre<Realtype>;
          break;
        case opq::LAGUERRE:
          wf= new wfLaguerre<Realtype>;
          break;
        case opq::CHEBYSHEV:
          wf = new wfChebyshev<Realtype>;
          break;
        case opq::HERMITE:
          wf = new wfHermite<Realtype>;
          break;
      }
    } else
    {
      switch(polynomialType){
        case opq::LEGENDRE:
        default:
          wf = new wfLegendre<Realtype>;
          break;
        case opq::LAGUERRE:
          wf= new wfLaguerreAlt<Realtype>;
          break;
        case opq::CHEBYSHEV:
          wf = new wfChebyshevAlt<Realtype>;
          break;
        case opq::HERMITE:
          wf = new wfHermiteAlt<Realtype>;
          break;
      }
    }
    OrthPol.setWeightfunction(wf);

    OrthPol.setQuadrature(quadraturetype, N_Nodes, Scaling);
    Arraytype ab(2, 2*N_Coeff-1);
    Arraytype exact(2, N_Coeff);
    exact = OrthPol.getExactCoefficients(polynomialType, N_Coeff);
    ab.setZero();
    switch(polynomialType)
    {
    case opq::LEGENDRE:
    default:
      ab(0,0) = 0.0;
      ab(1,0) = 0.0;
      break;
    case opq::LAGUERRE:
      ab(0,0) = 1.0;
      ab(1,0) = 0.0;
      break;
    case opq::CHEBYSHEV:
      ab(0,0) = 0.0;
      ab(1,0) = 0.25;
      break;
    case opq::HERMITE:
      ab(0,0) = 0.0;
      ab(1,0) = 0.0;
    }

    OrthPol.computeChebyshev(ab, N_Coeff, N_iterations);

    Arraytype result(2, N_Coeff);
    result = OrthPol.coefficients();

    Matrix<Realtype, Dynamic, Dynamic> Error(2, N_Coeff);
    Error = result;
    Error -= exact;
    for(int i = 0; i< N_Coeff; i++)
    {
      if(exact(0,1) != 0) Error(0,i) /= exact(0,i);
      if(exact(1,i) != 0) Error(1,i) /= exact(1,i);
    }
    Error = Error.cwiseAbs();
    std::cout << "Error: \n" << Error.block(0,0,2,N_Coeff) << "\n Norm: " << Error.block(0,0,2,N_Coeff).cwiseAbs().maxCoeff() << std::endl << std::endl;

    VERIFY(Error.block(0,0,2,N_Coeff).cwiseAbs().maxCoeff() < eps);

    delete wf;
  }

  template<class TestFunction>
  void TestQuadrature(typename OPQ<Realtype, Arraytype>::orthPolType quadraturetype,
                      const int& N_Nodes,
                      const Realtype& a,
                      const Realtype& b,
                      const Realtype& exactResult,
                      bool Scaling = true,
                      const Realtype& alpha = 0)
  {
    typedef OPQ<Realtype, Arraytype> opq;
    typedef Matrix<Realtype, Dynamic, 1> Vec;
    QuadratureFormula<Realtype> *Quadrature = NULL;
    switch(quadraturetype)
    {
    case opq::LEGENDRE:
    default:
      Quadrature = new GaussLegendre<Realtype>(N_Nodes);
      break;
    case opq::LAGUERRE:
      Quadrature = new GaussLaguerre<Realtype>(N_Nodes, alpha);
      break;
    case opq::CHEBYSHEV:
      Quadrature = new GaussChebyshev<Realtype>(N_Nodes);
      break;
    case opq::HERMITE:
      Quadrature = new GaussHermite<Realtype>(N_Nodes);
      break;
    }

    Vec nodes(N_Nodes);
    Vec weights(N_Nodes);
    nodes = Quadrature->getNodes();
    weights= Quadrature->getWeights();


    Realtype transf1, transf2;
    Realtype lbound= Quadrature->getLBound(), ubound = Quadrature->getUBound();

    if(lbound!=ubound){
      transf1 = (b-a)/(ubound-lbound);
      transf2 = (a*ubound-b*lbound)/(ubound-lbound);
    } else{
      transf1 = 1.0;
      transf2 = 0.0;
    }

    TestFunction testfunc;
    Vec f_eval(N_Nodes);
    Vec ScaleFactors(N_Nodes);
    Quadrature->setScaling(Scaling);
    ScaleFactors = Quadrature->getScaleFactors();

    for(int i = 0; i<N_Nodes; i++)
    {
      f_eval(i) = transf1*testfunc.eval(transf1*nodes(i)+transf2)*ScaleFactors(i);
    }

    Realtype result = weights.dot(f_eval);

    std::cout.precision(16);
    std::cout << "result:  " << result << ",   exact:  " << exactResult << std::endl;

    VERIFY_IS_APPROX(result, exactResult);

    delete Quadrature;
  }
private:

};



void test_OPQ() 
{
  typedef double Realtype;
  typedef OPQ<Realtype, Matrix<Realtype,Dynamic,Dynamic> > opq;

  OPQTests<Realtype, Matrix<Realtype,Dynamic,Dynamic> > opqtest;

  std::cout << "\n### GAUSSIAN QUADRATURE FORMULA TEST ### \n\n";
  
  /* Integrate Polynomial1 from -2 to 2. Exact solution is 78.4
   * Polynomial1 is of order n=9, Gauss-Legendre Quadrature with N nodes
   * must be exact for n <=  2*N-1
   */
  opqtest.TestQuadrature<Polynomial1<Realtype> >(opq::LEGENDRE, 3,-2, 2, 78.4);
  
  /* Integrate Polynomial2 from 0 to 10. Exact solution is -312/35
   * Polynomial2 is of order n=6. Gauss-Chebyshev Quadrature with 256 Nodes
   */
  opqtest.TestQuadrature<Polynomial2<Realtype> >(opq::CHEBYSHEV, 256, 0, 2, -312.0/35.0);
  
  /* Integrate exp(x) from -1 to 1. The exact solution is 2.3504023872876029137647 */
  opqtest.TestQuadrature<ExponentialFunction<Realtype> >(opq::LEGENDRE, 20, -1, 1, 2.3504023872876029137647);
  
  opqtest.TestQuadrature<NegativeExponentialFunction<Realtype> >(opq::LAGUERRE, 10, 0,0,1, true, 0);
  
  opqtest.TestQuadrature<Polynomial2<Realtype> >(opq::LAGUERRE, 4, 0,0,768, false);

  opqtest.TestQuadrature<ExpHermiteFunction<Realtype> >(opq::HERMITE, 128, 0,0,1.772453850905516027298167483, true);
  //TODO: Test Laguerre Quadrature with alpha != 0
  opqtest.TestQuadrature<Polynomial1<Realtype> >(opq::HERMITE, 5,-2, 2, 37, false);




  std::cout << "\n\n### CHEBYSHEV ALGORITHM TESTS ### \n\n";
  
  /* Test the modified Chebyshev algorithm with exact recursion 
   * coefficients for Legendre polynomials as input
   */
  std::cout << "++Gauss-Legendre for Legendre polynomials with 512 (default) N_nodes, exact coefficients as start coefficients and Scaling = false:\n";
  opqtest.TestCase(12, 1, opq::LEGENDRE, opq::LEGENDRE, 1e-10, 512, false);
  std::cout << "++Gauss-Legendre for Legendre polynomials with 512 (default) N_nodes, exact coefficients as start coefficients and Scaling = true:\n";
  opqtest.TestCase(12, 1, opq::LEGENDRE, opq::LEGENDRE, 1e-10, 512, true);
  std::cout << "++Gauss-Legendre for Legendre polynomials with 512 (default) N_nodes, zero coefficients as start coefficients and Scaling = false::\n";
  opqtest.TestCaseZeroCoeff(12, 1, opq::LEGENDRE, opq::LEGENDRE, 1e-9, 512, false);
  std::cout << "++Gauss-Legendre for Legendre polynomials with 512 (default) N_nodes, zero coefficients as start coefficients and Scaling = true:\n";
  opqtest.TestCaseZeroCoeff(12, 1, opq::LEGENDRE, opq::LEGENDRE, 1e-9, 512, true);

  
  std::cout << "++Gauss-Laguerre for Laguerre polynomials with 20 (default) nodes, exact coefficients as start coefficients and Scaling = false: \n";
  opqtest.TestCase(10, 1, opq::LAGUERRE, opq::LAGUERRE, 1e-4, 20, false);
  std::cout << "++Gauss-Laguerre for Laguerre polynomials with 20 (default) nodes, exact coefficients as start coefficients and Scaling = true:\n";
  opqtest.TestCase(10, 1, opq::LAGUERRE, opq::LAGUERRE, 1e-4, 20, true);
  std::cout << "++Gauss-Laguerre for Laguerre polynomials with 20 (default) nodes, zero coefficients as start coefficients and Scaling = false: \n";
  opqtest.TestCaseZeroCoeff(10, 1, opq::LAGUERRE, opq::LAGUERRE, 1e-4, 20, false);
  std::cout << "++Gauss-Laguerre for Laguerre polynomials with 20 (default) nodes, zero coefficients as start coefficients and Scaling = true:\n";
  opqtest.TestCaseZeroCoeff(10, 1, opq::LAGUERRE, opq::LAGUERRE, 1e-4, 20, true);


  std::cout << "++Legendre polynomials with exact moments and exact coefficients as start coefficients:\n";
  opqtest.TestExactMoments(20, opq::LEGENDRE, 1e-14);


  std::cout << "++Gauss-Chebyshev for Chebyshev polynomials with 512 (default) nodes, exact coefficients as start coefficients and Scaling = false:\n";
  opqtest.TestCase(12, 1, opq::CHEBYSHEV, opq::CHEBYSHEV, 1e-8, 512, false);
  std::cout << "++Gauss-Chebyshev for Chebyshev polynomials with 512 (default) nodes, exact coefficients as start coefficients and Scaling = true:\n";
  opqtest.TestCase(12, 1, opq::CHEBYSHEV, opq::CHEBYSHEV, 1e-5, 512, true);
  std::cout << "++Gauss-Chebyshev for Chebyshev polynomials with 512 (default) nodes, zero coefficients as start coefficients and Scaling = false:\n";
  opqtest.TestCaseZeroCoeff(12, 1, opq::CHEBYSHEV, opq::CHEBYSHEV, 1e-8, 512, false);
  std::cout << "++Gauss-Chebyshev for Chebyshev polynomials with 512 (default) nodes, zero coefficients as start coefficients and Scaling = true:\n";
  opqtest.TestCaseZeroCoeff(12, 1, opq::CHEBYSHEV, opq::CHEBYSHEV, 1e-5, 512, true);


  std::cout << "++Gauss-Hermite for Hermite polynomials with 100 (default) nodes, exact coefficients as start coefficients and Scaling = false:\n";
  opqtest.TestCase(10, 1, opq::HERMITE, opq::HERMITE, 1e-10, 100, false);
  std::cout << "++Gauss-Hermite for Hermite polynomials with 100 (default) nodes, exact coefficients as start coefficients and Scaling = true:\n";
  opqtest.TestCase(10, 1, opq::HERMITE, opq::HERMITE, 1e-10, 100, true);
  std::cout << "++Gauss-Hermite for Hermite polynomials with 100 (default) nodes, zero coefficients as start coefficients and Scaling = false:\n";
  opqtest.TestCaseZeroCoeff(10, 1, opq::HERMITE, opq::HERMITE, 1e-5, 100, false);
  std::cout << "++Gauss-Hermite for Hermite polynomials with 100 (default) nodes, zero coefficients as start coefficients and Scaling = true:\n";
  opqtest.TestCaseZeroCoeff(10, 1, opq::HERMITE, opq::HERMITE, 1e-5, 100, true);


  std::cout << "++Hermite polynomials with exact moments and exact coefficients as start coefficients:\n";
  opqtest.TestExactMoments(20, opq::HERMITE, 1e-15);


  std::cout << "++Clenshaw Curtis for Legendre polynomials with 512 (default) nodes, exact coefficients as start coefficients and Scaling = true::\n";
  opqtest.TestCase(12, 1, opq::LEGENDRE, opq::CLENSHAW_CURTIS, 1e-10, 512, true);
  std::cout << "++Clenshaw Curtis for Legendre polynomials with 512 (default) nodes, zero coefficients as start coefficients and Scaling = true::\n";
  opqtest.TestCaseZeroCoeff(12, 1, opq::LEGENDRE, opq::CLENSHAW_CURTIS, 1e-6, 512, true);
  std::cout << "++Clenshaw Curtis for Chebyshev polynomials with 512 (default) nodes, exact coefficients as start coefficients and Scaling = true::\n";
  opqtest.TestCase(12, 1, opq::CHEBYSHEV, opq::CLENSHAW_CURTIS, 1e-6, 512, true);
  std::cout << "++Clenshaw Curtis for Chebyshev polynomials with 512 (default) nodes, zero coefficients as start coefficients and Scaling = true::\n";
  opqtest.TestCaseZeroCoeff(12, 1, opq::CHEBYSHEV, opq::CLENSHAW_CURTIS, 1e-6, 512, true);

}
