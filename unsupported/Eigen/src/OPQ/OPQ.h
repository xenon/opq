// Copyright (C) 2014 Yannick Dueren, Julian Koellermeier, Roman Schaerer
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef EIGEN_OPQ_H
#define EIGEN_OPQ_H

#include<iostream>

namespace Eigen {

//Forward Declarations
template<typename Realtype> class WeightFunction;
template<typename Realtype> class QuadratureFormula;
template<typename Realtype> class ClassicalQuadratureFormula;
template<typename Realtype, typename Arraytype> class OPQ;

/**
 * \ingroup OPQ_Module
 * \class WeightFunction
 * \brief Abstract base class for weight functions
 *
 * This class is an abstract base class for weight functions.
 * In order to hand a weight function to the class OPQ, you have to
 * inherit from this class and have to specify:
 *  - The function \ref eval. This function should then return the
 *    weight function evaluated at x.
 *  - The intervall \f$ [a,b] \f$ of the weight function in the class constructor.
 *
 * If the intervall \f$ [a,b] \f$ is unbounded (e.g. \f$ [0,\inf) \f$),
 * set \f$ a = b \f$ (e.g. \f$ a = 0 \f$ and \f$ b = 0 \f$).
 *
 *
 *  For example the weight function \f$ w(x) = \sqrt{1-x^2} \f$ on the
 *  intervall \f$ [-1,1] \f$ can be stored as:
 *
 *  \code{.cpp}
 *    template<typename Realtype>
 *    class w : public WeightFunction<Realytype> {
 *      public:
 *      w(){ this->a=-1; this->b=1;}
 *      inline Realtype eval(Realtype x) {
 *        return 1/sqrt(1-x^2);
 *      }
 *    };
 *  \endcode
 *
 * If your weight funciton \f$ w(x) \f$ can be split into \f$ w(x) = f(x)*g(x) \f$, where
 * \f$ g \f$ is a classical weight function of one of the types of OPQ::orthPolType,
 * you may consider implementing just \f$ f(x) \f$ as the weight function.
 * This avoids the "Scaling" of the Quadrature. For further information see
 * \ref OPQ::setQuadrature.
 *
 */
template<typename Realtype>
class WeightFunction
{
public:
  virtual ~WeightFunction(){}
  /**
   * \brief Evaluates the Weight function at multiple points
   * \param [in] x : Vector of points to evaluate the weight function at.
   * \returns the result of the weight function evaluated at the points x.
   */
  Matrix<Realtype, Dynamic, 1> WFEval(const Matrix<Realtype, Dynamic, 1>& x)
  {
    const int N = x.rows();
    Matrix<Realtype, Dynamic, 1> result(N);
    for(int i = 0; i<N; i++)
    {
      result(i,0) = this->eval(x(i, 0));
    }
    return result;
  }

  /**
   * \brief evaluate the weight function at a point
   * \return the result of the weight function evaluated at the point x.
   *
   * This is function must be implemented by the user by inheriting
   * from the class WeightFunction.
   */
  virtual Realtype eval(Realtype) = 0;
  Realtype a, b;
};

/**
 * \ingroup OPQ_Module
 * \class QuadratureFormula
 * \brief Abstract base class for Gaussian quadrature formulas
 *
 * This class is an abstract base class for Gaussian quadrature formulas.
 * In order to hand a Gaussian Quadrature formula to the class OPQ, you have to
 * inherit from this class and have to specify:
 *  - The function \ref computeQuadrature, which calculates nodes and weights.
 *  - The standard integration intervall \f$ [lbound,ubound] \f$ of the
 *    Gaussian quadrature in the class constructor by setting the member
 *    variables \ref lbound and \ref ubound.
 *
 *
 * \em Scaling:
 *  In this module Scaling describes the following:
 *  Gaussian Quadrature is a quadrature formula of the form
 *  \f$ \int\limits_{d_1}^{d_2}f(t)d\mu(t) \approx \sum\limits_{i=1}^{n}A_{i}f(\tau_{i}). \f$
 *  where \f$ d\mu(x) = \omega(x)*dx \f$ is a given measure and \f$ A_i \f$ weights and \f$ \tau_i \f$
 *  nodes of the quadrature formula.
 *  If you want to integrate a function \f$ g \f$ with this Gaussian quadrature formula,
 *  which is \em not of the form \f$ g(x) = f(x) * \omega(x) \f$ you have to evaluate
 *  the function \f$ \tilde{g}(x) = g(x) * \omega^{-1}(x) \f$ at the nodes to get
 *  the correctly approximated result. We call this "scaling".
 *
 *  Scaling is enabled by default in the constructor, so you need to disable it,
 *  if you do not want to use this feature. This can be done by simply not calling
 *  the QuadratureFormula constructor in your inherited quadrature formula and
 *  setting this->QuadratureScaling to false or
 *  calling the function \ref setScaling.
 *
 * This class already provides some utility functions:
 *  - \ref golubWelsh is the Golub Welsh algorithm
 *    for generating nodes and weights for the quadrature.
 *  - \ref getScaleFactors returns Factors for "Scaling"
 *    of the quadrature (see above)
 *
 * There are some already implemented quadrature formulas provided:
 *  - \ref GaussLegendre (default N_Nodes : 512)
 *  - GaussLaguerre (default N_Nodes : 20)
 *  - GaussChebyshev (default N_Nodes : 512)
 *  - GaussHermite (default N_Nodes : 100)
 *  .
 * A special already implemented quadrature formula is
 * \em Clenshaw-Curtis. You can use this class with
 * \ref OPQ::setQuadrature(orthPolType, int, bool).
 * The default number of nodes and weights for
 * this quadrature is 512.
 *
 * There is a class for classical pre-implemented quadrature
 * formulas: \ref ClassicalQuadratureFormula.
 *
 * For an example see the implementation of \ref GaussLegendre,
 * which provides a pre-implemented Gauss-Legendre quadrature
 * formula.
 */
template<typename Realtype>
class QuadratureFormula
{
protected:
  typedef Matrix<Realtype, Dynamic, 1> Vec; ///< Eigen vector datatype for elements of type \p Realtype
  typedef Matrix<Realtype, Dynamic, Dynamic> Mat; ///< Eigen Matrix datatype for elements of type \p Realtype

public:
  QuadratureFormula() : QuadratureScaling(true){}
  virtual ~QuadratureFormula(){}

  /**
   * \brief Recompute Nodes and Weights
   *
   * Nodes and weights of the quadrature formula are calculated. The member
   * variable \ref N_Nodes is set to \p n_nodes.
   *
   * \param [in] n_nodes : number of nodes to be calculated
   */
  void recompute(int n_nodes)
  {
    this->N_Nodes = n_nodes;
    computeQuadrature();
  }


  /**
   * \brief The Golub-Welsh algorithm for generating nodes and weights
   *  for a Gaussian quadrature formula
   *
   * This Function uses the Golub-Welsh algorithm to calculate nodes and weights
   * for a Gaussian quadrature formula. These nodes and weights will be saved
   * in member variables of this class. They can be returned by \ref getNodes and
   * \ref getWeights.
   *
   * \remark
   * This function will generate nodes and weights
   * for a Gaussian Quadrature:
   * The zeros of the \em{n}-th orthogonal polynomial with respect
   * to a positive measure \f$ d\mu \f$, hence the nodes \f$ \tau_i \f$ in the
   * Gaussian Quadrature rule, are the eigenvalues of the matrix:
   * \f{align*}
   *   \[ \mathbf{A} = \begin{bmatrix}
   *    \alpha_0 	      & \sqrt{\beta_1}&        	      &             &          \\
   *    \sqrt{\beta_1}  & \alpha_1	    & \sqrt{\beta_2}&             &          \\
   * 	                  & \sqrt{\beta_2}& \alpha_2      & \ddots      &          \\
   *                    &        	      & \ddots 	      & \ddots	    &          \\
   *                    &        	      &        	      &             &
   *   \end{bmatrix} \]
   * \f}
   * This matrix is constructed by the coefficients \f$ \alpha_k \f$ and
   * \f$ \beta_k \f$ of the three-term recurrence relation for the monic
   * orthogonal polynomials \f$ p_k(d\mu;t) \f$.
   * The corresponing weights \f$ A_i \f$ are given by
   * \f$ A_i = \mu_0*(\phi_{1}^{(i)})^{2}, \f$, where \f$ \phi_{1}^{(i)} \f$
   * is the first entry of the i-th eigenvector.
   *
   * \param [in] ab : A 2x\p N_Nodes Array of recursion coefficients
   *  \f$ (\alpha_k, \beta_k) \f$. The nodes and weights will be constructed
   *  from these.
   * \param [in] mu_0 : The calculated moment \f$ \mu_0 = \int\limits_R d\mu(x) \f$
   *  where \f$ \mu(x)\f$ is the weight function for the orthogonal
   *  polynomials.
   */
  void golubWelsh(Mat ab, Realtype mu_0)
  {
    SelfAdjointEigenSolver<Mat> EigenSolver;
    EigenSolver.computeFromTridiagonal(ab.row(0), ab.row(1).tail(this->N_Nodes-1).cwiseAbs().cwiseSqrt());
    this->Nodes = EigenSolver.eigenvalues();
    this->Weights= mu_0 * (EigenSolver.eigenvectors().row(0).cwiseProduct(EigenSolver.eigenvectors().row(0))).transpose();
  }

  /**
   * \brief Set the number of nodes (and weights).
   *
   * The number of nodes and weights of the quadrature formula
   * (\ref N_Nodes) is set with this function. In Order to
   * calculate the nodes and weights the number must be set beforehand.
   *
   * \param [in] n_nodes : Number of nodes and weights
   */
  void setNNodes(int n_nodes)
  {
    this->N_Nodes = n_nodes;
    this->Nodes.resize(n_nodes);
    this->Weights.resize(n_nodes);
    this->computeQuadrature();
  }

  /**
   * \brief Enable/Disable Scaling.
   *
   * To enable "Scaling" set it to true, to disable false.
   * For a explanation of "Scaling" see \ref QuadratureFormula
   *
   * \param [in] Scaling : Boolean to Enable/Disable Scaling
   */
  void setScaling(bool Scaling)
  {
    this->QuadratureScaling = Scaling;
  }

  /**
   * \brief Get Scale Factors
   * \returns Vector of Factors to scale an integrand.
   *
   * If Scaling is enabled this function calculates a vector of Scale factors with
   * the member function \ref ScaleFactors. Elsewise a vector of ones
   * is returned (this equals no Scaling). These Scale factors are the
   * inverse of the weight function of the quadrature evaluated at the nodes.
   *
   * If you are using a Gaussian quadrature to approximate an integral (see
   * \ref QuadratureFormula), you can easily do this by evaluating the integrand
   * at all nodes and building the dot product with the Scale factors:
   * Let \f$ A \f$ be a vector of the weights of the quadrature, \f$ h \f$ be
   * a vector of the integrand evaluated at the nodes and \f$ s \f$ be
   * the vector of the Scale factors. A Gaussian quadrature is the
   * achieved by multiplying \f$ A \f$ with \f$ h \f$ component-wise and then
   * building the dot product with \f$ s \f$
   *
   * For a explanation of "Scaling" see \ref QuadratureFormula
   **/
  virtual Vec getScaleFactors()
  {
    Vec Scale(this->N_Nodes);
    if(this->QuadratureScaling)
    {
      Scale = this->ScaleFactors();
    }
    else
    {
      Scale.fill(1.0);
    }
    return Scale;
  }

  /**
   * \brief Return nodes.
   * \pre The nodes have been calculated.
   * \returns Vector of calculated nodes of a quadrature formula.
   **/
  Vec getNodes() const
  {
    return this->Nodes;
  }

  /**
   * \brief Return weights.
   * \pre The weights have been calculated.
   * \returns Vector of calculated nodes of a quadrature formua.
   **/
  Vec getWeights() const
  {
    return this->Weights;
  }

  /**
   * \brief Return lower bound of the standard integration intervall.
   *
   * For example: For an integration on \f$ (-3,8] \f$ the function returns -3.
   **/
  Realtype getLBound() const
  {
    return this->lbound;
  }

  /**
   * \brief Return upper bound of the standard integration intervall.
   *
   * For example: For an integration on \f$ (-3,8], \f$ the function returns 8.
   **/
  Realtype getUBound() const
  {
    return this->ubound;
  }

  /**
   * \brief Return number of nodes and weights of the quadrature.
   **/
  int getNNodes(){
    return this->N_Nodes;
  }

  /**
   * \brief Returns if Scaling is enabled.
   * \return boolean, if Scaling is enabled.
   *
   * For an explanation of "Scaling" see \ref QuadratureFormula
   */
  bool isScalingEnabled()
  {
    return this->QuadratureScaling;
  }

protected:
  /**
   * \brief Compute nodes and weights of the quadrature formula.
   *
   * \pre the Number of nodes and weights (QuadratureFormula::N_Nodes) must
   *  be set.
   *
   * This function computes the nodes and weights of the quadrature formula.
   * They will be saved in member variables QuadratureFormula::Nodes and
   * QuadratureFormula::Weights, both of type QuadratureFormula::Vec (Matrix<Realtype, Dynamic, 1>),
   * of this class. They can be returned by getNodes() and getWeights().
   *
   * This is function must be implemented by the user by inheriting
   * from the class WeightFunction.
   */
  virtual void computeQuadrature() = 0;

  /**
   * \brief Calculates the Scale Factors
   *
   * This function evaluates the inverse of the weight function of the
   * quadrature. The default implementation sets
   * every element of the vector to 1 (no Scaling, even if it is enabled).
   * To add your own implementation of Scaling re-implement this virtual
   * function.
   *
   * For an explanation of "Scaling" see \ref QuadratureFormula
   *
   * \returns A vector of Scale Vectors
   */
  virtual Vec ScaleFactors()
  {
    Vec Scale(this->N_Nodes);
    Scale.fill(1.0);
    return Scale;
  }

  int N_Nodes; ///< Number of nodes (and weights) to be calculated or already calculated
  bool QuadratureScaling; ///< If set to true, Scaling is enabled. See \ref QuadratureFormula
  Realtype lbound; ///< Lower bound of the standard integration intervall
  Realtype ubound; ///< Upper bound of the standard integration intevall
  Vec Nodes; ///< Nodes of the quadrature formula
  Vec Weights; ///< Weights of the quadrature formula
};

/**
 * \ingroup OPQ_Module
 * \class ClassicalQuadratureFormula
 * \brief Abstract base class for specific pre-implemented
 *  classical Gaussian quadrature formulas.
 *
 * It mainly provides the new function \ref getCoefficients.
 *
 * The pre-implemented quadratures (and their default
 * number of nodes) are:
 *  - \ref GaussLegendre (default \ref N_Nodes : 512)
 *  - GaussLaguerre (default \ref N_Nodes : 20)
 *  - GaussChebyshev (default \ref N_Nodes : 512)
 *  - GaussHermite (default \ref N_Nodes : 100)
 *
 * For using one of the pre-implemented QuadratureFormula
 * you do not have to create an object of it
 * by yourself, you can simply use the function \ref
 * OPQ::setQuadrature(orthPolType type, int n_nodes, bool Scaling).
 *
 *
 * The pre-implemented quadrature formula ClenshawCurtis is
 * a special case. It does not inherit from this class, but
 * is a child class of \ref QuadratureFormula.
 *
 * For an example see the documentation of \ref GaussLegendre,
 * which provides a pre-implemented Gauss-Legendre quadrature
 * formula.
 */
template<typename Realtype>
class ClassicalQuadratureFormula : public QuadratureFormula<Realtype>
{
protected:
  typedef Matrix<Realtype, Dynamic, 1> Vec; ///< The used Vector datatype
  typedef Matrix<Realtype, Dynamic, Dynamic> Mat; ///< The used Matrix datatype

public:
  ClassicalQuadratureFormula() : QuadratureFormula<Realtype>() {}

  virtual ~ClassicalQuadratureFormula(){}
  /**
   * \brief Returns exact three-term recursion coefficients
   *
   * This function will calculate \p N three-term recursion coefficients.
   * It is implemented in child classes of ClassicalQuadratureFormula.
   *
   * \param [in] N : Number of recursion coefficients
   * \return Matrix of N exact three-term recusion coefficients.
   */
  virtual Mat getCoefficients(const int N) = 0;
protected:
  /**
   * \brief Compute nodes and weights of the quadrature formula.
   *
   * \pre the Number of nodes and weights \ref N_Nodes must
   *  be set.
   *
   * This function computes the nodes and weights of the quadrature formula.
   * They will be saved in member variables \ref Nodes and
   * \ref Weights, both of type \ref Vec,
   * of this class. They can be returned by \ref getNodes and
   * \ref getWeights.
   *
   * The computation is done by calculating three-term recursion
   * coefficients for the classical weight function of the
   * Gaussian quadrature formula and using the Gloub-Welsh
   * algorithm implemented in \ref golubWelsh.
   */
  void computeQuadrature()
  {
    this->Nodes.resize(this->N_Nodes, 1);
    this->Weights.resize(this->N_Nodes, 1);
    Mat ab = getCoefficients(this->N_Nodes);
    this->golubWelsh(ab, this->mu0);
  }

  Realtype mu0; /**< The calculated moment \f$ \mu_0 = \int\limits_R d\mu(x) \f$
   *  where \f$ \mu(x)\f$ is the weight function for the orthogonal
   *  polynomials.
   */
};

/**
 * \ingroup OPQ_Module
 * \class GaussLegendre
 * \brief A Gauss-Legendre quadrature formula
 *
 * The documenation for this class is an example for
 * the pre-implemented classes for Gaussian quadrature
 * formulas:
 *  - Gauss-Legendre
 *  - GaussLaguerre
 *  - GaussChebyshev
 *  - GaussHermite
 *  .
 * The implementation of these algorihms are analog.
 *
 * This class provides a Gaussian quadrature formula
 * based on classical Legendre polynomials.
 * The Legendre weight function is \f$ w(x) = 1 \f$.
 * The function \ref getCoefficients is implemented
 * and returns recursion coefficients for Legendre
 * polynomials. The function \ref computeQuadrature
 * uses these recursion coefficients and the in
 * \ref QuadratureFormula implemented Golub-Welsh
 * algorithm to compute the nodes and weights for a
 * Gauss-Legendre quadrature formula.
 *
 * The default number of nodes generated is 512.
 * You can choose \ref N_Nodes with the function
 * \ref setNNodes.
 *
 * For the documentation on how to implement your own
 * quadrature formula see \ref GaussianQuadrature.
 *
 * \sa ClassicalQuadratureFormula.
 */
template<typename Realtype>
class GaussLegendre : public ClassicalQuadratureFormula<Realtype>
{
  typedef Matrix<Realtype, Dynamic, 1> Vec;
  typedef Matrix<Realtype, Dynamic, Dynamic> Mat;

public:
  /**
   * \brief GaussLegendre constructor
   * \param [in] n_nodes : Number of nodes and weights.
   * Can be changed later with \ref setNNodes. The default
   * value is 512.
   */
  GaussLegendre(int n_nodes = 512) : ClassicalQuadratureFormula<Realtype>()
  {
    this->N_Nodes = n_nodes;
    this->mu0 = 2.0;
    this->lbound = -1.0;
    this->ubound = 1.0;
    this->computeQuadrature();
  }
  ~GaussLegendre(){}
  virtual Mat getCoefficients(const int N)
  {
    Mat ab(2, N);
    ab.row(0).setZero();
    ab(1,0) = 0;
    for(Realtype i = 1.0; i<N; i++)
    {
      ab(1,i) = 1.0/(4.0-1.0/(i*i));
    }
    return ab;
  }
protected:
  /**
   * \brief Calculates the Scale Factors
   *
   * For this class the concrete implementation of
   * ScaleFactors will just return a vector of ones,
   * since \f$ w(x) = 1 \f$ is the weight function
   * (respectively the inverse of the weight function)
   * of the quadrature.
   *
   * @returns Vector of scale factors.
   */
  Vec ScaleFactors()
  {
    Vec Scale(this->N_Nodes);
    Scale.fill(1.0);
    return Scale;
  }
};

template<typename Realtype>
class GaussLaguerre : public ClassicalQuadratureFormula<Realtype>
{
  typedef Matrix<Realtype, Dynamic, 1> Vec;
  typedef Matrix<Realtype, Dynamic, Dynamic> Mat;

public:
  //You can use generalised Gauss-Laguerre Quadrature by choosing _alpha != 0
  GaussLaguerre(int n_nodes = 20, Realtype _alpha = 0) : ClassicalQuadratureFormula<Realtype>()
  {
    eigen_assert(_alpha > -1);
    this->alpha = _alpha;
    this->N_Nodes = n_nodes;
    this->mu0 = 1.0;
    this->lbound = 0.0;
    this->ubound = 0.0;
    this->computeQuadrature();
  }
  ~GaussLaguerre(){}
  virtual Mat getCoefficients(const int N)
  {
    eigen_assert(this->alpha > -1);
    Mat ab(2, N);
    for(Realtype i = 0.0; i<N; i++)
    {
      ab(0,i) = 2.0*i+1.0+this->alpha;
      ab(1,i) = i*i+i*this->alpha;
    }
    return ab;
  }

protected:
  Vec ScaleFactors()
  {
    Vec Scale(this->N_Nodes);
    if(this->alpha == 0)
    {
      for(int i = 0; i<this->N_Nodes; i++)
      {
        Scale(i, 0) = std::exp(this->Nodes(i, 0));
      }
    }
    else
    {
      for(int i = 0; i<this->N_Nodes; i++)
      {
        Scale(i, 0) = (std::pow(this->Nodes(i,0), -this->alpha))*std::exp(this->Nodes(i, 0));
      }
    }
    return Scale;
  }
  Realtype alpha;
};

template<typename Realtype>
class GaussChebyshev : public ClassicalQuadratureFormula<Realtype>
{
  typedef Matrix<Realtype, Dynamic, 1> Vec;
  typedef Matrix<Realtype, Dynamic, Dynamic> Mat;
public:
  GaussChebyshev(int n_nodes = 512) : ClassicalQuadratureFormula<Realtype>()
  {
    this->N_Nodes = n_nodes;
    this->mu0 = 1.5707963267948966192313216916397514420985846996875529;
    this->lbound = -1.0;
    this->ubound = 1.0;
    this->computeQuadrature();
  }
  ~GaussChebyshev(){}
  virtual Mat getCoefficients(const int N)
  {
    Mat ab(2, N);
    ab.row(0).setZero();
    ab.row(1).fill(0.25);
    return ab;
  }
protected:
  Vec ScaleFactors()
  {
    Vec Scale(this->N_Nodes);
    for(int i = 0; i<this->N_Nodes; i++)
    {
      Scale(i, 0) = 1.0/std::sqrt(1-this->Nodes(i,0)*this->Nodes(i,0));
    }
    return Scale;
  }
};

template<typename Realtype>
class GaussHermite : public ClassicalQuadratureFormula<Realtype>
{
  typedef Matrix<Realtype, Dynamic, 1> Vec;
  typedef Matrix<Realtype, Dynamic, Dynamic> Mat;

public:
  GaussHermite(int n_nodes = 100) : ClassicalQuadratureFormula<Realtype>()
  {
    this->N_Nodes = n_nodes;
    this->mu0 = 1.7724538509055160272981674833411451827975494561223871;
    this->lbound = 0.0;
    this->ubound = 0.0;
    this->computeQuadrature();
  }
  ~GaussHermite(){}
  virtual Mat getCoefficients(const int N)
  {
    Mat ab(2, N);
    ab.row(0).setZero();
    for(Realtype i = 0.0; i<N; i++)
    {
      ab(1,i) = i/2.0;
    }
    return ab;
  }
protected:
  Vec ScaleFactors()
  {
    Vec Scale(this->N_Nodes, 1);
    for(int i = 0; i<this->N_Nodes; i++)
    {
      Scale(i,0) = std::exp(this->Nodes(i,0) * this->Nodes(i,0));
    }
    return Scale;
  }
};

/* GaussJacobi
template<typename Realtype>
class GaussJacobi : public ClassicalQuadratureFormula<Realtype>
{
  typedef Matrix<Realtype, Dynamic, 1> Vec;
  typedef Matrix<Realtype, Dynamic, Dynamic> Mat;
public:
  GaussJacobi(int n_nodes = 20, Realtype _alpha = 0) : ClassicalQuadratureFormula<Realtype>()
  {
    eigen_assert(alpha > -1);
    this->alpha = _alpha;
    this->N_Nodes = n_nodes;
    this->mu0 = 1.0;
    this->lbound = 0.0;
    this->ubound = 0.0;
    this->computeQuadrature();
  }
  ~GaussJacobi(){}
  virtual Mat getCoefficients(const int N)
  {
    eigen_assert(this->alpha > -1);
    Mat ab(2, N);
    for(Realtype i = 0.0; i<N; i++)
    {
      ab(0,i) = 2.0*i+1.0+this->alpha;
      ab(1,i) = i*i+i*this->alpha;
    }
    return ab;
  }

protected:
  Vec ScaleFactors()
  {
    Vec Scale(this->N_Nodes);
    if(this->alpha == 0)
    {
      for(int i = 0; i<this->N_Nodes; i++)
      {
        Scale(i, 0) = std::exp(this->Nodes(i, 0));
      }
    }
    else
    {
      for(int i = 0; i<this->N_Nodes; i++)
      {
        Scale(i, 0) = (std::pow(this->Nodes(i,0), -this->alpha))*std::exp(this->Nodes(i, 0));
      }
    }
    return Scale;
  }
  Realtype alpha;
  Realtype beta;
};
*/

template<typename Realtype>
class ClenshawCurtis : public QuadratureFormula<Realtype>
{
public:
  ClenshawCurtis(int n_nodes = 512)
  {
    this->N_Nodes = n_nodes;
    this->lbound = -1.0;
    this->ubound = 1.0;
    this->computeQuadrature();
  }
  ~ClenshawCurtis(){}

  void computeQuadrature()
  {
    const Realtype pi = 3.1415926535897932384626433832795028841971693993751058;
    this->Nodes.resize(this->N_Nodes, 1);
    this->Weights.resize(this->N_Nodes, 1);

    // Initialise Nodes
    // nodes = cos(pi*(0:n)’/n);
    for(Realtype i = 0; i<this->N_Nodes; i++){
      this->Nodes(i) = std::cos(pi*i/(this->N_Nodes -1.0));
    }

    //Initialise Weights
    // weights = 0*a’; w(1:2:end) = 2./(1-(0:2:n).ˆ2);
    this->Weights.setZero();
    for(Realtype i = 0; i<this->N_Nodes; i+=2.0)
    {
      this->Weights(i) = 2.0/(1.0-i*i);
    }
  }
};

/**
 * \ingroup OPQ_Module
 * \class OPQ
 * \brief Provides several exact recursion coefficients
 *  for classical orthogonal Polynomials and the
 *  modified Chebyshev algorithm.
 *
 * This class contains several methods returning recursion coefficients
 * \f$(\alpha_k, \beta_k)\f$ for orthogonal Polynomials that satisfy the
 * three-term recurrence relation
 * \f{align*}
 *   P_{k+1}(t) = (t - \alpha_{k})P_{k}(t) - \beta_{k}P_{k-1}(t),   k = 0,1,2,...
 * \f}
 *
 * Tools are provided for:
 *  - Exact three-term recursion coefficients for often used orthogonal Polynomials,
 *  - Approximating three-term recursion coefficients with the modified Chebyshev
 *    algorithm.
 *
 * Exact three-term recursion coefficients can be generated for:
 *  - Legendre polynomials (#LEGENDRE)
 *  - Laguerre polynomials (#LAGUERRE)
 *  - Chebyshev polynomials (second kind) (#CHEBYSHEV)
 *  - Hermite polynomials (#HERMITE)
 *
 * Approximating three-term recursion coefficients for a weight function
 * with the modified Chebyshev algorithm:
 * You need to inherit of the class \ref WeightFunction to implement your
 * weight function. Then set this weight function with
 * \ref setWeightfunction(WeightFunction<Realtype>*). If you do not have the
 * exact modified moments of the weight function, you need to set a
 * quadrature formula to approximate them. Inherit the class \ref QuadratureFormula
 * to specify your quadrature (see \ref QuadratureFormula) and set it with
 * \ref OPQ::setQuadrature(QuadratureFormula<Realtype>*)
 * or choose one of the provided Gaussian/Clenshaw-Curtis quadrature
 * formulas with \ref setQuadrature(orthPolType, int, bool).
 *
 *
 * \tparam Realtype The underlying data type (typically float or double)
 * \tparam Arraytype The underlying Array Taype
 *  (typically Eigen::Matrix<Realtype, Dynamic, 1>)
 *
 *
 **/
template<typename Realtype, typename Arraytype>
class OPQ
{
  typedef Matrix<Realtype, 2, Dynamic> ABM;
  typedef Matrix<Realtype, Dynamic, 1> Vec;
  typedef Matrix<Realtype, Dynamic, Dynamic> Mat;


public:
  /**
   * \brief The orthPolType enum
   *
   * This enum is used to identify different types of classical orthogonal
   * polynomials and/or quadrature types.
   **/
  enum orthPolType
  {
    LEGENDRE, ///< Legendre polynomials/Gauss-Legendre quadrature
    LAGUERRE, ///< Laguerre polynomials/Gauss-Laguerre quadrature
    CHEBYSHEV, ///<Chebyshev polynomials/Gauss-Chebyshev quadrature
    HERMITE, ///< Hermite polynomials/Gauss-Hermite quadrature
    CLENSHAW_CURTIS ///< This type is just for setting a Clenshaw-Curtis quadrature
  };

  OPQ()
  {
    this->quadratureformula = NULL;
    this->weightfunction = NULL;
    this->CC_Quadrature = false;
    this->N_Coeff = 0;
  }
  ~OPQ(){}

  /**
   * \brief The modified Chebyshev Algorithm for approximating three-term recurrence
   *  coefficients
   *
   * The modified Chebyshev algorithm approximates three-term recurrence coefficients
   * \f$ \alpha_{n}, \beta_{n} \f$ of the orthogonal polynomials of a weight function
   * by evaluating so-called modified moments. This is done by encoding the
   * weight function by its first 2*\p n_coeff moments relative to monic
   * polynomials defined by \p abm, which are "close" in some sense to the
   * desired polynomials.
   *
   * \pre This function requires:
   *  - The Weight function, you want to approximate the recursion coefficients for,
   *  has to be set. This can be done with the function setWeightfunction().
   *  - A Gaussian (or Clenshaw-Curtis) Quadrature formula to approximate the moments.
   *  This can be set with setQuadrature().
   *
   * \param [in] abm : 2*\p N_coeff-1 recurrence coefficients \f$ a \f$
   *  and \f$ b \f$ "close" to the ones to be generated, stored in a
   *  2x(2*\p N_coeff -1) array. The first coefficients \f$ a_0 \f$ and
   * \f$ b_0 \f$ schould be exact.
   *
   *  The following parameters are optional:
   * \param [in] n_coeff : number of recursion coefficients to be generated. If
   *  not given, the function will calculate \p n_coeff by callling abm.cols(),
   *  which for this case has to be implemented for Arraytype
   * \param [in] N_iterations : number of iterations of the modified
   *  Chebyshev algorithm
   *
   **/
  void computeChebyshev(const Arraytype& abm, const int n_coeff = -1, const int N_iterations=1)
  {
    eigen_assert(this->quadratureformula != NULL);
    eigen_assert(this->weightfunction != NULL);


    //Set N_Coeff
    if(n_coeff < 0 )
    {
      this->N_Coeff = (abm.cols()+1)/2;
    }
    else
    {
      this->N_Coeff = n_coeff;
    }

    this->Coefficients.resize(2, this->N_Coeff);
    //Alternative: this->Coefficients = abm;

    //Copy abm to internal Arraytype
    this->Coeff.resize(2, 2*this->N_Coeff-1);
    for(int i = 0; i < 2*this->N_Coeff-1; i++)
    {
      this->Coeff(0, i) = abm(0,i);
      this->Coeff(1, i) = abm(1,i);
    }

    for(int i = 0; i<N_iterations; i++)
    {
      SingleChebyshev();
    }

    //Copy Data back to external Arraytype
    for(int i = 0; i < this->N_Coeff; i++)
    {
      this->Coefficients(0, i) = this->Coeff(0,i);
      this->Coefficients(1, i) = this->Coeff(1,i);
    }
  }

  template<typename HPType>
  void computeChebyshev(const Matrix<Realtype, Dynamic, Dynamic>& abm_Realtype, const int n_coeff = -1)
  {
    eigen_assert(this->quadratureformula != NULL);
    eigen_assert(this->weightfunction != NULL);

    typedef Matrix<HPType, Dynamic, Dynamic> HPArray;

    //Set N_Coeff
    if(n_coeff < 0 )
    {
      this->N_Coeff = (abm_Realtype.cols()+1)/2;
    }
    else
    {
      this->N_Coeff = n_coeff;
    }

    HPArray abm(2, 2*N_Coeff-1);
    for(int i = 0; i<2*N_Coeff-1; i++)
    {
      abm(0,i) = abm_Realtype(0,i);
      abm(1,i) = abm_Realtype(1,i);
    }
    HPArray ab(2, N_Coeff);

    HPArray sigma(this->N_Coeff+1, 2*this->N_Coeff);
    for(int i = 0; i<2*N_Coeff; i++)
    {
      sigma(0,i) = 0;
    }

    Vec Moments(2*this->N_Coeff);
    this->computeMoments(abm_Realtype, Moments);

    for(int i = 0; i<2*N_Coeff; i++)
    {
      sigma(1,i) = Moments(i);
    }

    ab(0,0) = abm(0,0) + sigma(1,1)/sigma(1,0);
    ab(1,0) = abm(1,0);

    /* Gautschi's algorithm */
    for (int n = 2; n< this->N_Coeff+1; n++)
    {
      for(int m = n-1; m<2*this->N_Coeff-n+1; m++)
      {
        sigma(n,m) = sigma(n-1,m+1) - (ab(0,n-2)-abm(0,m))*sigma(n-1,m) - ab(1,n-2)*sigma(n-2,m) + abm(1,m) * sigma(n-1,m-1);
      }

      ab(0, n-1)=abm(0,n-1)+sigma(n,n)/sigma(n,n-1)-sigma(n-1,n-1)/sigma(n-1,n-2);
      ab(1, n-1)=sigma(n,n-1)/sigma(n-1,n-2);
    }

    this->Coefficients.resize(2, N_Coeff);
    //Copy Data back to external Arraytype
    for(int i = 0; i < this->N_Coeff; i++)
    {
      this->Coefficients(0, i) = static_cast<Realtype>(ab(0,i));
      this->Coefficients(1, i) = static_cast<Realtype>(ab(1,i));
    }
  }


  /**
   * \brief The modified Chebyshev Algorithm for approximating three-term recurrence
   *  coefficients
   *
   * The modified Chebyshev algorithm approximates three-term recurrence coefficients
   * \f$ \alpha_{n}, \beta_{n} \f$ of the orthogonal polynomials of a weight function
   * by evaluating so-called modified moments. This is done by encoding the weight function
   * by its first 2*\p n_coeff moments relative to monic polynomials defined
   * by \p abm, which are "close" in some sense to the desired polynomials.
   *
   * \pre This function requires:
   *  - The Weight function, you want to approximate the recursion coefficients for, has to
   *  be set. This can be done with the function setWeightfunction().
   *  - A Gaussian (or Clenshaw-Curtis) Quadrature formula to approximate the moments.
   *  This can be set with setQuadrature().
   *
   * \param [in] alpha : 2*\p N_coeff -1 recurrence coefficients \f$ a \f$
   *  "close" to the ones to be generated, stored in a
   *  2x(2*\p N_coeff -1) array. The first coefficient \f$ a_0 \f$ schould be exact.
   * \param [in] beta : 2*\p N_coeff-1 recurrence coefficients \f$ b \f$
   * corresponding to \p alpha. The first coefficient \f$ b_0 \f$ schould be exact.
   *
   *  The following parameters are optional:
   * \param [in] n_coeff : number of recursion coefficients to be generated. If
   *  not given, the function will calculate \p n_coeff by callling alpha.rows(),
   *  which for this case has to be implemented for \p Arraytype.
   * \param [in] N_iterations : number of iterations of the modified Chebyshev algorithm
   *
   **/
  //TODO: Test this function
  void computeChebyshev(const Vec& alpha, const Vec& beta, const int n_coeff = -1, const int N_iterations=1)
  {
    eigen_assert(this->quadratureformula != NULL);
    eigen_assert(this->weightfunction != NULL);


    //Set N_Coeff
    if(n_coeff < 0 )
    {
      this->N_Coeff = (alpha.rows()+1)/2;
    }
    else
    {
      this->N_Coeff = n_coeff;
    }

    this->Coefficients.resize(2, this->N_Coeff);

    //Copy abm to internal Arraytype
    this->Coeff.resize(2, 2*this->N_Coeff-1);
    for(int i = 0; i < 2*this->N_Coeff-1; i++)
    {
      this->Coeff(0, i) = alpha(i);
      this->Coeff(1, i) = beta(i);
    }

    for(int i = 0; i<N_iterations; i++)
    {
      SingleChebyshev();
    }

    //Copy Data back to external Arraytype
    for(int i = 0; i < this->N_Coeff; i++)
    {
      this->Coefficients(0, i) = this->Coeff(0,i);
      this->Coefficients(1, i) = this->Coeff(1,i);
    }
  }

  /**
   * \brief The modified Chebyshev Algorithm for exactly calculated moments
   *
   * The modified Chebyshev algorithm approximates three-term recurrence coefficients
   * \f$ \alpha_{n}, \beta_{n} \f$ of the orthogonal polynomials of a weight function
   * by evaluating so-called modified moments. This function provides this algorithm
   * for pre-calculated moments.
   *
   * \pre The Weight function, you want to approximate the recursion coefficients for, has to
   *  be set. This can be done with the function
   *  setWeightfunction(WeightFunction<Realtype>*).
   *
   * \param [in] abm : 2*\p N_coeff-1 recurrence coefficients "close" to the ones
   *  to be generated, stored in a 2x(2*\p N_coeff -1) array
   * \param [in] Moments : the weight function encoded by its first 2*\p N_coeff
   *  moments stored as a row vector relative to monic polynomials defined
   *  by \p abm.
   */
  void computeChebyshevFromMoments(const Arraytype& abm, const Arraytype& _moments)
  {
    this->N_Coeff = (abm.cols()+1)/2;
    //Copy to internal data structures
    this->Coeff.resize(2,2*N_Coeff-1);
    for(int i = 0; i<2*N_Coeff-1; i++)
    {
      this->Coeff(0,i) = abm(0,i);
      this->Coeff(1,i) = abm(1,i);
    }

    Vec Moments(2*this->N_Coeff);
    for(int i = 0; i<2*N_Coeff; i++)
    {
      Moments(i) = _moments(0,i);
    }
    Matrix<Realtype, Dynamic, Dynamic> sigma(N_Coeff+1, 2*N_Coeff);
    sigma.row(0).setZero();
    sigma.row(1) = Moments.transpose();
    ABM abm_old(this->Coeff);

    /*
    sigma.row(0).setZero();
    sigma.row(1) = Moments;

    Arraytype ab(abm.block(0,0,2,2*N_Coeff-1));
    ab(0,0) = abm(0,0) + sigma(1,1)/sigma(1,0);
    //~ abm(1,0) = Moments(0);
    ab(1,0) = abm(1,0);
    for (int n = 2; n< N_Coeff+1; n++)
    {
      for(int m = n-1; m<2*N_Coeff-n+1; m++)
      {
        sigma(n,m) = sigma(n-1,m+1) - (ab(0,n-2)-abm(0,m))*sigma(n-1,m) - ab(1,n-2)*sigma(n-2,m) + abm(1,m) * sigma(n-1,m-1);
      }

      ab(0, n-1)=abm(0,n-1)+sigma(n,n)/sigma(n,n-1)-sigma(n-1,n-1)/sigma(n-1,n-2);
      ab(1, n-1)=sigma(n,n-1)/sigma(n-1,n-2);
    }
    this->Coefficients = ab.block(0,0,2,N_Coeff);
    //Copy Data back to external Arraytype
    for(int i = 0; i < this->N_Coeff; i++)
    {
      this->Coefficients(0, i) = ab(0,i);
      this->Coefficients(1, i) = ab(1,i);
    }
    */


    this->Coeff(0,0) = abm_old(0,0) + sigma(1,1)/sigma(1,0);
    this->Coeff(1,0) = abm_old(1,0);

    /* Gautschi's algorithm */
    for (int n = 2; n< this->N_Coeff+1; n++)
    {
      for(int m = n-1; m<2*this->N_Coeff-n+1; m++)
      {
        sigma(n,m) = sigma(n-1,m+1) - (this->Coeff(0,n-2)-abm_old(0,m))*sigma(n-1,m) - this->Coeff(1,n-2)*sigma(n-2,m) + abm_old(1,m) * sigma(n-1,m-1);
      }

      this->Coeff(0, n-1)=abm_old(0,n-1)+sigma(n,n)/sigma(n,n-1)-sigma(n-1,n-1)/sigma(n-1,n-2);
      this->Coeff(1, n-1)=sigma(n,n-1)/sigma(n-1,n-2);
    }

    this->Coefficients.resize(2,N_Coeff);
    //Copy Data back to external Arraytype
    for(int i = 0; i < this->N_Coeff; i++)
    {
      this->Coefficients(0, i) = this->Coeff(0,i);
      this->Coefficients(1, i) = this->Coeff(1,i);
    }
  }


  /**
   * \brief Generates exact three-term recurrence coefficients for polynomials of
   * the given type.
   * \returns 2x\p N Array (of type \p Arraytype) exact three-term recurrence coefficients.
   *
   * Following recursion coefficents are provided:
   *  - Legendre (type = #LEGENDRE)
   *  - Laguerre (type = #LAGUERRE)
   *  - Chebyshev (second kind) (type = #CHEBYSHEV)
   *  - Hermite (type = #HERMITE)
   *
   * \param [in] type : type of polynomials
   * \param [in] N : Number of coeffients to be generated
   **/
  Arraytype getExactCoefficients(orthPolType type, int N)
  {
    eigen_assert(type != CLENSHAW_CURTIS);

    ClassicalQuadratureFormula<Realtype>* quad = NULL;
    Arraytype ab(2, N);

    switch(type){
    case LEGENDRE:
    default:
      quad = new GaussLegendre<Realtype>;
      ab = quad->getCoefficients(N);
      break;
    case LAGUERRE:
      quad = new GaussLaguerre<Realtype>;
      ab = quad->getCoefficients(N);
      break;
    case CHEBYSHEV:
      quad = new GaussChebyshev<Realtype>;
      ab = quad->getCoefficients(N);
      break;
    case HERMITE:
      quad = new GaussHermite<Realtype>;
      ab = quad->getCoefficients(N);
      break;
    }
    return ab;
  }

  /**
   * \brief returns calculated recursion coefficients \f$ \alpha_k \f$
   * \returns row vector of the calculated three-term recurrence relation recursion
   *  coefficients \f$ \alpha_k \f$.
   *
   * \pre The recursion coefficients have been computed before.
   *
   * To use this function Arraytype must provide a function row(int k) for returning the
   * k-th row of the Array.
   **/
  Arraytype alphas()
  {
    return this->Coefficients.row(0);
  }

  /**
   * \brief returns calculated recursion coefficients \f$ \beta_k \f$
   * \returns row vector of the calculated three-term recurrence relation recursion
   *  coefficients \f$ \beta_k \f$.
   *
   * \pre The recursion coefficients have been computed before.
   *
   * To use this function Arraytype must provide a function row(int k) for returning the
   * k-th row of the Array.
   **/
  Arraytype betas()
  {
    return this->Coefficients.row(1);
  }

  /**
   * \brief Returns Calculated recursion coefficients
   * \returns 2xN Array of the calculated N three-term recurrence relation recursion
   *  coefficients \f$ (\alpha_k, \beta_k) \f$.
   *
   * \pre The recursion coefficients have been computed before.
   *
   **/
  Arraytype coefficients()
  {
    return this->Coefficients;
  }

  /**
   * \brief Set the quadrature formula
   *
   * In order to use the modified Chebyshev algorithm without knowing the exact
   * modified moments, you need to set a quadrature formula. See
   * \ref QuadratureFormula on how to implement your own quadrature formula.
   *
   * You must ensure, that the quadrature formula, the pointer is
   * referencing, is not destroyed during the usage of OPQ.
   *
   * \param [in] QF : pointer to the quadrature formula.
   **/
  void setQuadrature(QuadratureFormula<Realtype>* QF)
  {
    this->quadratureformula = QF;
  }

  /**
   * \brief Set the quadrature formula
   *
   * In order to use the modified Chebyshev algorithm without knowing the exact
   * modified moments, you need to set a quadrature formula. See
   * \ref QuadratureFormula on how to implement your own quadrature formula.
   *
   * You must ensure, that the the quadrature formula, the pointer is
   * referencing, is not destroyed during the usage of OPQ.
   *
   * \param [in] QF : pointer to the quadrature formula.
   **/
  void setQuadrature(ClassicalQuadratureFormula<Realtype>* QF)
  {
    this->quadratureformula = QF;
  }

  /**
   * \brief Set the Quadrature formula to a pre-implemented one
   *
   * Set the quadrature formula that will be used to calculate the modieifed moments
   * in the Chebyshev algorithm. You have to choose a quadrature of type orthPolType.
   *
   * Following quadrature formulas are provided:
   *  - Gauss-Legendre (type = #LEGENDRE)
   *  - Gauss-Laguerre (type = #LAGUERRE)
   *  - Gauss-Chebyshev (type = #CHEBYSHEV)
   *  - Gauss-Hermite (type = #HERMITE)
   *  - Clenshaw-Curtis (type = #CLENSHAW_CURTIS)
   *
   * The quadratures can be used in two different forms:
   *  -# Let \f$ w(x) \f$ be the weight funciton you want to calculate recursion
   * coefficients for. If \f$ w \f$ can be split into: \f$ w(x) = f(x)*g(x) \f$, where
   * \f$ g \f$ is a classical weight function of one of the types above you can:
   *    - Hand this class the weight function \f$ f(x) \f$ with setWeightfunction()
   *    - Set \p Scaling = false
   *    .
   *  -# If \f$ w(x) \f$ cannot be split (or you do not want so):
   *    - Hand this class the weight function \f$ w(x) \f$ with setWeightfunction()
   *    - Set \p Scaling = true.
   *    - The algorithm will now compute the modified moments by integrating some
   *      polynomials \f$ P_k(x) \f$ and the weight function by evaluating
   *      \f$ \int{P_k(x)*w(x)*\mu(x)^{-1}dx} \f$ where \f$ \mu(x) \f$ is the classical
   *      weight function of the quadrature formula (e.g. for Gauss-Laguerre quadrature
   *      this is \f$ \mu(x) = \exp(-x) \f$).
   *    .
   * .
   *
   * For further explanations of Scaling see \ref QuadratureFormula.
   *
   * The default number of nodes for these quadratures are:
   *  - Gauss-Legendre: 512
   *  - Gauss-Laguerre: 20
   *  - Gauss-Chebyshev: 512
   *  - Gauss-Hermite: 100
   *  - Clenshax-Curtis: 512
   *
   * \param [in] type : the quadrature type
   * \param [in] n_nodes : Number of nodes for the quadrature
   * \param [in] Scaling : If true, the weight function is divided by the classical
   * weight function of the quadrature formula (see above)
   */
  void setQuadrature(orthPolType type, int n_nodes = -1, bool Scaling = true)
  {
    if(n_nodes < 0)
      //default number of Nodes for the Quadrature
    {
      switch(type){
      case LEGENDRE:
      default:
        this->quadratureformula = new GaussLegendre<Realtype>;
        this->CC_Quadrature = false;
        break;
      case LAGUERRE:
        this->quadratureformula = new GaussLaguerre<Realtype>;
        this->CC_Quadrature = false;
        break;
      case CHEBYSHEV:
        this->quadratureformula = new GaussChebyshev<Realtype>;
        this->CC_Quadrature = false;
        break;
      case HERMITE:
        this->quadratureformula = new GaussHermite<Realtype>;
        this->CC_Quadrature = false;
        break;
      case CLENSHAW_CURTIS:
        this->quadratureformula = new ClenshawCurtis<Realtype>;
        this->CC_Quadrature = true;
        break;
      }
    }
    else
      //User defined number of nodes for the Quadrature
    {
      switch(type){
      case LEGENDRE:
      default:
        this->quadratureformula = new GaussLegendre<Realtype>(n_nodes);
        this->CC_Quadrature = false;
        break;
      case LAGUERRE:
        this->quadratureformula = new GaussLaguerre<Realtype>(n_nodes);
        this->CC_Quadrature = false;
        break;
      case CHEBYSHEV:
        this->quadratureformula = new GaussChebyshev<Realtype>(n_nodes);
        this->CC_Quadrature = false;
        break;
      case HERMITE:
        this->quadratureformula = new GaussHermite<Realtype>(n_nodes);
        this->CC_Quadrature = false;
        break;
      case CLENSHAW_CURTIS:
        this->quadratureformula = new ClenshawCurtis<Realtype>(n_nodes);
        this->CC_Quadrature = true;
        break;
      }
    }
    this->quadratureformula->setScaling(Scaling);
  }

  /**
   * \brief Set the weight function
   *
   * In order to use the modified Chebyshev algorithm you have to set the
   * weight function. Inherit of the class WeightFunction, specify
   * your weight function and hand a pointer to OPQ with this function.
   *
   * For further information see \ref WeightFunction.
   *
   * You must ensure, that the the weight function, the pointer is
   * referencing, is not destroyed during the usage of OPQ.
   *
   * \param [in] wf : pointer to the weight function.
   **/
  void setWeightfunction(WeightFunction<Realtype>* wf)
  {
    weightfunction = wf;
  }

private:

  void SingleChebyshev()
  {
    Mat sigma(this->N_Coeff+1, 2*this->N_Coeff);
    sigma.row(0).setZero();
    Vec Moments(2*this->N_Coeff);

    this->computeMoments(this->Coeff, Moments);
    sigma.row(1) = Moments.transpose();
    ABM abm_old(this->Coeff);

    this->Coeff(0,0) = abm_old(0,0) + sigma(1,1)/sigma(1,0);
    //(Gautschi: abm(1,0) = Moments(0);)
    this->Coeff(1,0) = abm_old(1,0);

  /*
    // Algorithm from "Modified Chebyshev algorithm: some applications" by Andrade, Ranga
    for(int k = 1; k< N_coeff; k++)
    {
      for(int l = 0; l< 2*N_coeff-2*k; l++)
      {
        sigma(k+1, l) = sigma(k, l+2) + (abm_old(0, k+l)-abm(0, k-1)) * sigma(k,l+1) - abm(1,k-1) * sigma(k-1,l+2) + abm_old(1,k+l) * sigma(k,l);
      }
      abm(0, k) = abm_old(0,k) + sigma(k+1,1)/sigma(k+1,0) - sigma(k,1)/sigma(k,0);
      abm(1, k) = sigma(k+1,0)/sigma(k,0);
    }
  */

    /* Gautschi's algorithm */
    for (int n = 2; n< this->N_Coeff+1; n++)
    {
      for(int m = n-1; m<2*this->N_Coeff-n+1; m++)
      {
        sigma(n,m) = sigma(n-1,m+1) - (this->Coeff(0,n-2)-abm_old(0,m))*sigma(n-1,m) - this->Coeff(1,n-2)*sigma(n-2,m) + abm_old(1,m) * sigma(n-1,m-1);
      }

      this->Coeff(0, n-1)=abm_old(0,n-1)+sigma(n,n)/sigma(n,n-1)-sigma(n-1,n-1)/sigma(n-1,n-2);
      this->Coeff(1, n-1)=sigma(n,n-1)/sigma(n-1,n-2);
    }
  }

  void computeMoments(const Mat abm, Vec& result)
  {
    const int N_Moments = 2*this->N_Coeff;

    //Save nodes and weights in this class to avoid overhead
    this->nodes = this->quadratureformula->getNodes();
    this->weights = this->quadratureformula->getWeights();
    const int N_Nodes = this->quadratureformula->getNNodes();

    //Initialise intervall transformation factors transf1 and transf2
    if(this->weightfunction->a < this->weightfunction->b)
    { //Finite Boundaries
      Realtype lbound = this->quadratureformula->getLBound(), ubound = this->quadratureformula->getUBound();
      Realtype a = this->weightfunction->a, b = this->weightfunction->b;
      this->transf1 = (b-a)/(ubound-lbound);
      this->transf2 = (a*ubound-b*lbound)/(ubound-lbound);
    } else
    { //Infinite Boundaries -> no intervall transformation
      this->transf1 = 1;
      this->transf2 = 0;
    }

    //Evauate Weight function at nodes
    this->wf_eval.resize(N_Nodes);
    this->WeightFunctionEval();
    Vec P_m1(N_Moments), P_m2(N_Moments), P(N_Moments);
    P.setZero();
    P_m1.setZero();
    P_m2.setZero();
    P_m1(0) = 1;

    if(!this->CC_Quadrature)
    /* Gaussian Quadrature */
    {
      Vec Moments(N_Moments),
          Poly_eval(N_Nodes);
      Vec ScaleFactors(N_Nodes);
      bool Scaling = this->quadratureformula->isScalingEnabled();
      //Gaussian Quadrature first moment
      this->PolynomialEval(P_m1, Poly_eval, 0);

      if(Scaling)
      {
        ScaleFactors = this->quadratureformula->getScaleFactors();
        Poly_eval = Poly_eval.cwiseProduct(ScaleFactors);
      }

      Moments(0) = this->transf1*this->weights.dot(Poly_eval.cwiseProduct(this->wf_eval));
      //Gaussian Quadrature remaining moments
      for (int l = 1; l < N_Moments; l++)
      {
        P << 0.0, P_m1.head(N_Moments-1);
        P = P - abm(0, l-1)*P_m1 - abm(1,l-1)*P_m2;
        this->PolynomialEval(P, Poly_eval, l);
        if(Scaling)
        {
          Poly_eval = Poly_eval.cwiseProduct(ScaleFactors);
        }
        //Gaussian Quadrature
        Moments(l) = this->transf1 * (this->weights.dot(Poly_eval.cwiseProduct(this->wf_eval)));
        P_m2 = P_m1;
        P_m1 = P;
      }

      result = Moments;
    }

    /* Clenshaw-Curtis Quadrature */
    else
    {
      const int N = N_Nodes-1;
      Vec Moments(N_Moments),
          Poly_eval(N_Nodes);

      //CC first moment
      //MATLAB: fx = feval(f,x)/(2*n);
      this->PolynomialEval(P_m1, Poly_eval, 0);
      Poly_eval = (Poly_eval.cwiseProduct(this->wf_eval))/(2*N);

      //MATLAB: g = real(fft(fx([1:n+1 n:-1:2])));
      FFT<Realtype> fft;
      Vec timevec(2*N);
      timevec.head(N+1) = Poly_eval.head(N+1);
      timevec.tail(N-1) = Poly_eval.segment(1,N-1).reverse();
      Matrix<std::complex<Realtype>, Dynamic, 1> g_tmp(2*N);
      fft.fwd(g_tmp,timevec);
      Vec g = g_tmp.real();

      //MATLAB: a = [g(1); g(2:n)+g(2*n:-1:n+2); g(n+1)];
      Vec a(N+1);
      a.setZero();
      a(0) = g(0);
      a.segment(1,N-1) = g.segment(1,N-1)+g.segment(N+1,N-1).reverse();
      a(N) = g(N);

      Moments(0) = transf1*weights.dot(a);

      //CC remaining moments
      for (int l = 1; l < N_Moments; l++)
      {
        P << 0.0, P_m1.head(N_Moments-1);
        P = P - abm(0, l-1)*P_m1 - abm(1,l-1)*P_m2;

        //Clenshaw-Curtis Quadrature
        //MATLAB fx = feval(f,x)/(2*n);
        this->PolynomialEval(P, Poly_eval, l);
        Poly_eval = (Poly_eval.cwiseProduct(wf_eval))/(2*N);

        //MATLAB g = real(fft(fx([1:n+1 n:-1:2])));
        FFT<Realtype> fft;
        Vec timevec(2*N);
        timevec.head(N+1) = Poly_eval.head(N+1);
        timevec.tail(N-1) = Poly_eval.segment(1,N-1).reverse();
        Matrix<std::complex<Realtype>, Dynamic, 1> g_tmp(2*N);
        fft.fwd(g_tmp,timevec);
        Vec g = g_tmp.real();

        //MATLAB a = [g(1); g(2:n)+g(2*n:-1:n+2); g(n+1)];
        Vec a(N+1);
        a(0) = g(0);
        a.segment(1,N-1) = g.segment(1,N-1)+g.segment(N+1,N-1).reverse();
        a(N) = g(N);

        Moments(l) = this->transf1*this->weights.dot(a);

        P_m2 = P_m1;
        P_m1 = P;
      }
      result = Moments;
    }
  }

  void PolynomialEval (const Vec& Poly, Vec& result, const int degree) const
  {
    Realtype x, Horner;
    const int N_Nodes = this->quadratureformula->getNNodes();
    for (int i = 0; i < N_Nodes; i++)
    {
      x = this->transf1*this->nodes(i)+this->transf2;
      Horner = Poly(degree);
      for (int k = degree-1; k > -1; k--)
      {
        Horner = Horner*x + Poly(k);
      }
      result(i) = Horner;
    }
  }

  void WeightFunctionEval ()
  {
    //TODO: Add Coordintate transformation transf1*nodes+transf2
    this->wf_eval = this->weightfunction->WFEval(transf1*this->nodes+transf2*Vec::Ones(this->nodes.size()));
    /*
    const int N_Nodes = this->quadratureformula->getNNodes();
    for (int i = 0; i < N_Nodes; i++)
    {
      this->wf_eval(i)= this->weightfunction->eval(this->transf1*this->nodes(i)+this->transf2);
    }
    */
  }


  Realtype transf1, transf2;
  Vec nodes, weights;
  Vec wf_eval;

  ABM Coeff;
  Arraytype Coefficients;
  int N_Coeff;
  bool CC_Quadrature;
  QuadratureFormula<Realtype>* quadratureformula;
  WeightFunction<Realtype>* weightfunction;
};

} // end namespace Eigen


#endif // EIGEN_OPQ_H
