#include "../../Eigen/Eigenvalues"
#include "../../unsupported/Eigen/OPQ"
#include <iostream>
//#include <boost/multiprecision/gmp.hpp>
#include <boost/multiprecision/cpp_bin_float.hpp>
//#include <boost/multiprecision/mpfr.hpp>
#include <unsupported/Eigen/MPRealSupport>

using std::sqrt;
using std::pow;
using namespace Eigen;
using namespace mpfr;
using namespace boost::multiprecision;


template <typename Realtype>
class wfLaguerre : public WeightFunction<Realtype>
{
public:
  wfLaguerre() {this->a = 0.0; this->b= 0.0;}
  Realtype eval(Realtype x){return x*x*std::exp(-x);}
};



template <typename Realtype>
class wfLaguerreAlt : public WeightFunction<Realtype>
{
public:
  wfLaguerreAlt() {this->a = 0.0; this->b= 0.0;}
  Realtype eval(Realtype x){return x*x;}
};

template <typename Realtype>
void testLaguerre(int N_Coeff, int N_Nodes, bool ZeroCoeff, bool Scaling)
{
  typedef Matrix<Realtype,Dynamic,Dynamic> Arraytype;
  typedef OPQ<Realtype, Arraytype> opq;
  WeightFunction<Realtype>* wf = NULL;
  OPQ<Realtype, Arraytype> OrthPol;

  //Set weight function
  if(Scaling)
  {
    wf= new wfLaguerre<Realtype>;
  } else {
    wf = new wfLaguerreAlt<Realtype>;
  }
  OrthPol.setWeightfunction(wf);

  //Prepare recursion coefficients
  Arraytype ab(2, 2*N_Coeff-1);
  Arraytype exact(2, 2*N_Coeff-1);
  GaussLaguerre<Realtype> gausslaguerre(20, 2);
  exact = gausslaguerre.getCoefficients(2*N_Coeff-1);
  if(ZeroCoeff) {
    ab.setZero();
    ab(0,0) = 1.0;
    ab(1,0) = 0.0;
  } else {
    ab = exact;
  }
  //set Quadrature
  OrthPol.setQuadrature(opq::LAGUERRE, N_Nodes, Scaling);

  OrthPol.computeChebyshev(ab, N_Coeff);

  Arraytype result(2, N_Coeff);
  result = OrthPol.coefficients();

  Matrix<Realtype, Dynamic, Dynamic> Error(3, N_Coeff);
  for(int i = 0; i<N_Coeff; i++)
  {
    Error(0,i) = i;
  }
  Error.block(1,0,2,N_Coeff) = result.block(0,0,2,N_Coeff);
  Error.block(1,0,2,N_Coeff) -= exact.block(0,0,2,N_Coeff);
  for(int i = 0; i< N_Coeff; i++)
  {
    if(exact(0,1) != 0) Error(1,i) /= exact(0,i);
    if(exact(1,i) != 0) Error(2,i) /= exact(1,i);
  }
  Error = Error.cwiseAbs();
  //std::cout.precision(20);
  //std::cerr << "Relative Error: \nIndex,\tAlpha,\tBeta,\n" << Error.block(0,0,3,N_Coeff).cwiseAbs().transpose() << "\n\n";
  std::cerr << "Relative Error: \nIndex,\tAlpha,\tBeta,\n";
  for(int i = 0;i<N_Coeff; i++)
  {
    std::cout  <<  Error(0,i) << "," << Error(1,i) << "," << Error(2,i) << "\n";
    // << std::fixed
  }
  std::cerr << "\n\n";
}




template <typename Realtype, typename HPType>
void testHighPrecision(int N_Coeff, int N_Nodes, bool ZeroCoeff, bool Scaling)
{
  typedef Matrix<Realtype,Dynamic,Dynamic> Arraytype;
  typedef OPQ<Realtype, Arraytype> opq;
  typedef Matrix<HPType,Dynamic,Dynamic> HPArray;

  WeightFunction<Realtype>* wf = NULL;
  OPQ<Realtype, Arraytype> OrthPol;

  //Set weight function
  if(Scaling)
  {
    wf= new wfLaguerre<Realtype>;
  } else {
    wf = new wfLaguerreAlt<Realtype>;
  }
  OrthPol.setWeightfunction(wf);

  //Prepare recursion coefficients
  Arraytype ab_Realtype(2,2*N_Coeff-1);
  Arraytype exact(2, 2*N_Coeff-1);
  GaussLaguerre<Realtype> gausslaguerre(20, 2);
  exact = gausslaguerre.getCoefficients(2*N_Coeff-1);
  //std::cout << exact.transpose() << "\n";

  if(ZeroCoeff) {
    for(int i = 0; i<2*N_Coeff-1; i++)
    {
      ab_Realtype(0,i) = 0.0;
      ab_Realtype(1,i) = 0.0;
    }
    ab_Realtype(0,0) = 1.0;
  } else {
    for(int i = 0; i<2*N_Coeff-1; i++)
    {
      ab_Realtype(0,i) = exact(0,i);
      ab_Realtype(1,i) = exact(1,i);
    }
  }
  //set Quadrature
  OrthPol.setQuadrature(opq::LAGUERRE, N_Nodes, Scaling);

  OrthPol.template computeChebyshev<HPType>(ab_Realtype, N_Coeff);

  Arraytype result(2, N_Coeff);
  result = OrthPol.coefficients();

  Matrix<Realtype, Dynamic, Dynamic> Error(2, N_Coeff);
  Error = result;
  Error -= exact.block(0,0,2, N_Coeff);
  for(int i = 0; i< N_Coeff; i++)
  {
    if(Error(0,1) != 0) Error(0,i) /= exact(0,i);
    if(Error(1,i) != 0) Error(1,i) /= exact(1,i);
  }
  Error = Error.cwiseAbs();
  std::cout << "Realtive Error: \n" << Error.block(0,0,2,N_Coeff).transpose() << "\n Norm: " << Error.block(0,0,2,N_Coeff).cwiseAbs().maxCoeff() << std::endl << std::endl;
}


int main(int argc, char* argv [])
{
  typedef long double Realtype;
  //mpreal::set_default_prec(256);


  std::cerr << "\n\n### CHEBYSHEV ALGORITHM TESTS, LAGUERRE POLYNOMIALS (alpha=2) ### \n\n";


  std::cerr << "++N_Nodes = 20, exact coefficients, Scaling = false: \n";
  testLaguerre<Realtype>(10, 20, false, false);
  std::cerr << "++N_Nodes = 20, exact coefficients, Scaling = true: \n";
  testLaguerre<Realtype>(10, 20, false, true);
  std::cerr << "++N_Nodes = 20, zero coefficients, Scaling = false: \n";
  testLaguerre<Realtype>(22, 20, true, false);
  std::cerr << "++N_Nodes = 20, zero coefficients, Scaling = true: \n";
  testLaguerre<Realtype>(10, 20, true, true);



  std::cerr << "++22 Coefficients: Datatype: long double, N_Nodes = 20, zero coefficients, Scaling = false: \n";
  testLaguerre<long double>(22, 20, true, false);

  std::cerr << "\n\n### HIGH PRECISION DATA TYPES, LAGUERRE POLYNOMIALS (alpha=2) ###\n\n";

  std::cerr << "++22 Coefficients: Datatype: long double, N_Nodes = 20, zero coefficients, Scaling = false: \n";
  testHighPrecision<long double, cpp_bin_float_double_extended >(22, 20, true, false);
  testHighPrecision<long double, number<backends::cpp_bin_float<300, backends::digit_base_2, void, boost::int16_t, -16382, 16383>, et_off> >(22, 30, true, false);
  return 0;
}
