\section{The OPQ Module for the Eigen library}
\label{sec:OPQ_module}

The package OPQ, written in C++, is a module for the Eigen library. It contains routines and classes for generating orthogonal polynomials and Gauss-type quadrature rules. It performs the construction of three-term recurrence coefficients )\eqref{eq:Three_term_recurrence}) and quadrature formulas as described in section \ref{sec:Gaussian_Quadrature}.
We will give a short description of the module and primarily the implemented functions.

\subsection{Implemented functions and classes}
\label{subsec:Implementation}

For all implemented functions and classes see the \emph{doxygen documentation} of the module. The main functions and classes are:

\begin{description}
\item[Class OPQ] This is the main class of the module. It provides several \emph{exact recursion coefficients} for classical orthogonal Polynomials and the modified Chebyshev algorithm. Important functions are:
\begin{description}
  \item[computeChebyshev] The modified Chebyshev algorithm (see \ref{subsec:Chebyshev_Algorithm}). The user must provide three-term recurrence coefficients for polynomials, which are in some way 'close' to the ones to be generated. $\alpha_0$ and $\beta_0$ must be exact (in most cases $ \alpha_i = \beta_i = 0$ for $i>0$ will work). It is also required to set a weight function and qudrature formula beforehand. The quadrature formula will be used to approximate the modified moments (\ref{eq:modified_moments}).
  \item[computeChebyshevFromMoments] This function provides the same alorithm as computeChebyshev, but for pre-calculated modified moments. Therefore it is not required to set a quadrature formula.
  \item[getExactCoeff] Returns exact three-term recursion coefficients $\alpha_k$ and $\beta_k$ (see \ref{sec:Orthogonal_Polynomials}) for classical Legendre, Laguerre, Chebyshev and Hermite polynomials.
\end{description}

\item[Class QuadratureFormula] This is an abstract base class for Gaussian quadrature formulas and in a special pre-implemented case for a Clenshaw-Curtis quadrature formula. Important functions are:
\begin{description}
  \item[computeQudrature] Computes weights and nodes of the quadrature formula. The user has to specify this function for his own Gaussian quadrature formula. 
  \item[golubWelsh] This function provides an implementation of the Golub-Welsh algorithm (\ref{subsec:Golub_Welsh}).
  \item[scaleFactors] This function provides factors to 'scale' an integrand: If you want to approximate an integral $\int\limits_{d_1}^{d_2}f(t)dt$ for a general function $f(t)$ with a Gaussian quadrature with respect to a positive measure $\omega(t)$ you can use the here so-called scaling. The function is multiplied by the inverse of the weight function to get the form $\int\limits_{d_1}^{d_2}f(t)\omega^{-1}(t)\omega(t)dt = \int\limits_{d_1}^{d_2}g(t)\omega(t)dt$ and to be able to integrate $f(t)$ with the Gaussian quadrature formula. The user has to implement this function, which returns the inverse of the weight function of the quadrature evaluated at a point.
\end{description}

\item[WeightFunction] This class is an abstract base class for weight functions. The user should inherit from this class and specify the weight function in the function \textbf{eval} to pass it to the class OPQ to be able to use the modified Chebyshev algorithm and calculate three-term recurrence coefficients with respect to the weight function. 
\end{description}

Further implemented classes are: \texttt{ClassicalQuadratureFormula}, an abstract base class for classical Gaussian quadrature formulas. \texttt{GaussLegendre, GaussChebyshev, GaussLaguerre, GaussHermite} and \texttt{ClenshawCurtis} are implemented classes for the corresponding quadrature formulas.  


\subsection{Example of a classical weight}
\label{subsec:Example}

In this section we consider a classical measure $d\mu = \omega(x)dx = x^2\exp(-x)dx$ on $\mathbb{R_+}$. For this classical Laguerre weight the three-term-recursion coefficients $\alpha_k(d\mu)$ and $\beta_k(d\mu)$ for $k = 0,1,...,n-1$ are (see \ref{eq:coeff_Laguerre})
$$\alpha_k =  2k+3,$$
$$\beta_k = k^2+2k,$$
$$k = 0,1,...,n-1.$$
The modified Chebyshev algorithm can be executed with the following commands:

\begin{lstlisting}[frame=single, language=C++]
typedef Matrix<double,Dynamic,Dynamic> Arraytype;
typedef OPQ<double, Arraytype> opq;
opq OrthPol;

//Set the weight function. Class wfLaguerre  is an
//implementation of the Laguerre weight function.
WeightFunction<double>* wf = new wfLaguerre;
OrthPol.setWeightfunction(wf);

//Compute modified Chebyshev algorithm
Arraytype ab(2, 2*N_Coeff-1);
OrthPol.setQuadrature(opq::LAGUERRE, N_Nodes, Scaling);
OrthPol.computeChebyshev(ab, N_Coeff);

//Return result
Arraytype result(2, N_Coeff);
result = OrthPol.coefficients();
\end{lstlisting}
We will now discuss the results we achieved with the following settings: The number of coefficients generated is 14. We used a Gauss-Laguerre quadrature with 20 nodes to approximate the modified moments. The start recursion coefficients are $$a_k = b_k = 0, k = 0,...,2N-2.$$ 
"Scaling" (see \ref{subsec:Implementation}) is disabled. 



\begin{figure}[tbph]
\centering
\begin{tikzpicture}
\begin{axis}[
	ymode=log,
	ylabel=Relative error of $\alpha_k$,
	xlabel=Coefficient k,
	scaled x ticks = false,
	legend style = {legend pos=north west, font=\tiny}
]
\addplot table[col sep = comma, x = Coefficients k, y = Relative Error Alpha_k]{figures/LaguerreFloat.csv};
\addlegendentry{Float}
\addplot table[col sep = comma, x =Coefficients k, y =Relative Error Alpha_k]{figures/LaguerreDouble.csv};
\addlegendentry{Double}
\addplot table[col sep = comma, x =Coefficients k, y =Relative Error Alpha_k]{figures/LaguerreLongDouble.csv};
\addlegendentry{Long Double}
\end{axis}
\end{tikzpicture}
\caption{Relative error of the recursion coefficients $\alpha_k$ for Laguerre polynomials for different floating point data types. The coefficients for float and $k>9$ are \texttt{nan}.}
\label{fig:LaguerreAlpha}
\end{figure}

\begin{figure}[tbph]
\centering
\begin{tikzpicture}
\begin{axis}[
	ymode=log,
	ylabel=Relative error of $\beta_k$,
	xlabel=Coefficient k,
	scaled x ticks = false,
	legend style = {legend pos=north west, font=\tiny}
]
\addplot table[col sep = comma, x = Coefficients k, y = Relative Error Beta_k]{figures/LaguerreFloat.csv};
\addlegendentry{Float}
\addplot table[col sep = comma, x =Coefficients k, y =Relative Error Beta_k]{figures/LaguerreDouble.csv};
\addlegendentry{Double}
\addplot table[col sep = comma, x =Coefficients k, y =Relative Error Beta_k]{figures/LaguerreLongDouble.csv};
\addlegendentry{Long Double}
\end{axis}
\end{tikzpicture}
\caption{Relative error of the recursion coefficients $\beta_k$ for Laguerre polynomials for different floating point data types. The coefficients for float and $k>9$ are \texttt{nan}.}
\label{fig:LaguerreBeta}
\end{figure}

\begin{figure}[tbph]
\centering
\begin{tikzpicture}
\begin{axis}[
	ymode=log,
	ylabel=Relative error,
	xlabel=Coefficient k,
	scaled x ticks = false,
	legend style = {legend pos=north west, font=\tiny}
]
\addplot table[col sep = comma, x = Coefficients k, y = Relative Error Alpha_k]{figures/LaguerreLongDoubleMore.csv};
\addlegendentry{$\alpha_k$}
\addplot table[col sep = comma, x =Coefficients k, y =Relative Error Beta_k]{figures/LaguerreLongDoubleMore.csv};
\addlegendentry{$\beta_k$}
\end{axis}
\end{tikzpicture}
\caption{Relative error of the recursion coefficients $\alpha_k$ and $\beta_k$ for Laguerre polynomials for the \texttt{long double} C++ data types.}
\label{fig:LaguerreLongDouble}
\end{figure}

In Figure \ref{fig:LaguerreAlpha} and \ref{fig:LaguerreBeta} one can see the relative errors for each of the recursion coefficients with respect to exactly calculated Coefficients for the different C++ floating point data types: \texttt{float}, \texttt{double} and \texttt{long double}. A plot of the error for the same configuration with the datatype long double, but for higher number of coefficients, is shown in Figure \ref{fig:LaguerreLongDouble}.

The modified Chebyshev algorithm is, as illustrated by Figures \ref{fig:LaguerreAlpha} to \ref{fig:LaguerreLongDouble} very \emph{ill-conditioned}. Small errors in the calculation of the modified moments will be highly amplified. As shown with these examples and further tests (see the unit tests of the OPQ++ module), the algorithm will only work properly for small numbers of nodes. If we can calculate \emph{exact modified moments}, it is possible to use the function \texttt{computeChebyshevFromMoments} to get\emph{ exact recursion coefficients} or very good approximations even for a higher number of nodes.

