\section{Introduction to Orthogonal Polynomials on $\mathbb{R}$}
\label{sec:Orthogonal_Polynomials}

Given a positive measure $d\mu(t)$ on $\mathbb{R}$ with finite or unbounded support, we define \emph{moments} 
  \begin{equation}
  	\label{eq:Oprth_Pol_moments}
    \mu_k = \int\limits_{\mathbb{R}}t^kd\mu(t).
  \end{equation}
If all $\mu_k$ exist and are finite, we can define an inner product 
$$\langle f,g\rangle = \int\limits_{\mathbb{R}}f(t)g(t)d\mu(t)$$
for all polynomials $f,g$.

A series of polynomials $(p_n), n \in \mathbb{N}$ with $\text{deg}(p_n) = n$ is called \emph{orthogonal}, if
  \begin{equation*}
    \langle p_j,p_k\rangle  = \begin{cases}
      \Vert p_j\Vert^2 & \textit{,if } j=k,\\
      0 & \textit{,if } j\neq k.
      \end{cases} 
  \end{equation*} 
and \emph{monic}, if $$p_n(t) = t^n + \text{terms of lower degree}.$$


It is known that monic orthogonal polynomials satisfy the \emph{three-term recurrence relation} (see \cite{Gautschi1})
\begin{equation}
  \label{eq:Three_term_recurrence}
  p_{n+1}(t) = (t-\alpha_n)p_n(t) - \beta_{n}p_{n-1}, \quad n = 0,1,\dots
\end{equation}
with the convention $p_0(t) = 1$ and $p_{-1}(t) = 0$. The coefficients are
\begin{equation}
\label{eq:exact_rec_coeff}
\alpha_{n} := \dfrac{\langle xp_{n}, p_{n}\rangle}{\langle p_{n}, p_{n}\rangle},\qquad \beta_{n} = \dfrac{\langle xp_{n}, p_{n}\rangle}{\langle p_{n-1}, p_{n-1} \rangle}.
\end{equation}

Monic orthogonal polynomials for continuous positive measures $d\mu(t) = \omega(t)dt$ are related to \emph{weight functions} $\omega(t)$ on $[d_1,d_2]$ (finite or infinite). We will consider some examples of classical orthogonal polynomials with positive measures:

\begin{enumerate}
  \item \emph{Legendre polynomials:} For the weight function $\omega(t) = 1$ on $[-1,1]$ the three-term recursion coefficients are for $k = 0,1,\dots$
\begin{equation}
\label{eq:coeff_Legendre}
  \alpha_k = 0,\qquad \beta_k = \dfrac{k^2}{4k^2-1}.
\end{equation}

  \item \emph{Chebyshev polynomials (second kind):} For the weight function $\omega(t) = \sqrt{1-t^2}$ on $[-1,1]$ the three-term recursion coefficients are for $k = 0,1,\dots$
\begin{equation}
\label{eq:coeff_Chebyshev}
   \alpha_k = 0,\qquad \beta_k = \dfrac{1}{4}. 
\end{equation}

  \item \emph{Laguerre polynomials:} For the weight function $\omega(t) = t^{\gamma}\exp(-t)$ on $[0,\infty)$ the three-term recursion coefficients are for $k = 0,1,\dots$
\begin{equation}
\label{eq:coeff_Laguerre}
  \alpha_k = 2k+1+\gamma,\qquad \beta_k = k^2+\gamma k.
\end{equation}

  \item \emph{Hermite polynomials:} For the weight function $\omega(t) = \exp(-t^{2})$ on $(-\infty,\infty)$ the three-term recursion coefficients are for $k = 0,1,\dots$
\begin{equation}
\label{eq:coeff_Hermite}
  \alpha_k = 0,\qquad \beta_k = \dfrac{k}{2}.
\end{equation} 
\end{enumerate}


We will exemplarily show the derivation of (\ref{eq:coeff_Legendre}):
Legendre polynomials satisfy the recursion formula (see \cite{Attar})
\begin{equation}
\label{eq:Legendre_recursion}
  (n+1)p_{n+1}(t) = (2n+1)tp_{n}(t) - np_{n-1}(t).
\end{equation}  
With $p_{n}(t) = c_{n}\tilde{p}_{n}(t)$ we can write:
\begin{align}
  (n+1)c_{n+1}\tilde{p}_{n+1} & =  (2n+1)tc_{n}\tilde{p}_{n} - nc_{n-1}\tilde{p}_{n-1},\nonumber\\
  \Leftrightarrow \dfrac{n+1}{2n+1}\dfrac{c_{n+1}}{c_{n}}\tilde{p}_{n+1} & =  t\tilde{p}_{n} - \dfrac{n}{2n+1}\dfrac{c_{n-1}}{c_{n}}\tilde{p}_{n-1}.
\end{align}
To get the coefficients $\alpha_{n}, \beta_{n}$ for a three-term recurrence relation (\ref{eq:Three_term_recurrence}) we compare the coefficients:
\begin{eqnarray*}
  \dfrac{n+1}{2n+1}\dfrac{c_{n+1}}{c_{n}}  \overset{!}{=} 1, \qquad 0  \overset{!}{=} \alpha_{n}, \qquad \dfrac{n}{2n+1}\dfrac{c_{n-1}}{c_{n}}  \overset{!}{=} \beta_{n},
\end{eqnarray*}
Considering the first equation:
\begin{eqnarray*}
   & c_{n+1} & =  \dfrac{2n+1}{n+1}c_{n}, \\
  \Leftrightarrow & c_{n}  & = \dfrac{2n-1}{2}c_{n-1}, \\
  \Leftrightarrow & \beta_{n} & = \dfrac{n}{2n+1} \dfrac{n}{2n-1} = \dfrac{n^{2}}{4n^{2} - 1}.
\end{eqnarray*}


Three-term recurrence coefficients can be computed with (\ref{eq:exact_rec_coeff}). To avoid the very high complexity of this method, one can use the \textit{modified Chebyshev algorithm} (method of modified moments).

\subsection{The modified Chebyshev algorithm}
\label{subsec:Chebyshev_Algorithm} 

The modified Chebyshev algorithm approximates three-term recurrence coefficients $\alpha_{n, \beta_{n}}$ by evaluating the so-called \emph{modified moments} (see \cite{MIL})
\begin{equation}
\label{eq:modified_moments}
\tilde{\mu}_{k} = \int\limits_{d1}^{d2} P_{k}(t) d\mu(t),
\end{equation}
where $P_{k}$ are polynomials in some sense 'close' to the desired polynomials $p_{n}$, which satisfy a three-term recurrence relation with coefficients $a_{k}, k = 1,2,\dots, 2n-1$ and $b_{k}, k = 1,2,\dots, 2n-1$.

\def\algbackskip{\hskip\dimexpr-\algorithmicindent+\labelsep}
\def\LState{\State \algbackskip}

\begin{algorithm}[H]
\caption{Modified Chebyshev algorithm (see \cite{Gautschi2} and \cite{Andrade})}\label{al:modchebyshev}
\begin{algorithmic}
\Procedure{Chebyshev}{}
\Statex
\LState \emph{\textsc{Initialization}}:
  \State $\text{Choose } a_l, l=1,2,\dots,2n-1 \text{ and } b_l, l=2,\dots,2n-1$
  \State $ P_{-1}(t)=0 $
  \State $P_0(t) = 1 $
  \State $P_l(t) = (t-a_l)P_{l-1}(t) - a_lP_{l-2}(t), \quad l = 1,2,\dots,2n-1$
  \State $\sigma_{-1,l} = 0, \quad l = 0,1,\dots,2n-1$
  \State $\sigma_{0,l} = \tilde{\mu}_l^{\Phi} = \int\limits_{d1}^{d2} P_l(t)d\Phi(t), \quad l = 0,1,\dots,2n-1$
  \State $\text{and }$
  \State $\alpha_{l}^{\Phi} = a_{1} + \dfrac{\sigma_{0,1}}{\sigma_{0,0}}, \qquad \beta_{1}^{\Phi} = 0$
\\
% \If {$i > \textit{stringlen}$} \Return false
% \EndIf
% \State $j \gets \textit{patlen}$
\LState \emph{\textsc{Continuation}}:
  \For{$k \gets 1,2,\dots,n-1$} 
    \For{$l \gets 0,1,\dots,2n-2k-1$}
      \State $\sigma_{k,l} = \sigma_{k-1,l+2}+ (a_{k+l+1}-\alpha_{k}^{\Phi})\sigma_{k-1,l+1}-\beta_{k}^{\Phi}\sigma_{k-2,l+2} + b_{k+l+1}\sigma_{k-1,l}$
    \EndFor
  \State $\textbf{end for}$
  \State $\alpha_{k+1}^{\Phi} = a_{k+1} + \frac{\sigma_{k,1}}{\sigma_{k,0}} - \frac{\sigma_{k-1,1}}{\sigma_{k-1,0}}, \qquad \beta_{k+1}^{\Phi} = \frac{\sigma_{k,0}}{\sigma_{k-1,0}}$
  \EndFor
  \State $\textbf{end for}$

\EndProcedure
\end{algorithmic}
\end{algorithm}

% \begin{eqnarray}
%     \phi_\alpha(\xi) &=& \delta(\xi-\xi_\alpha), \label{e:DVM_fail_basis_fcn} \\
%     \psi_\beta(\xi) &=& \delta(\xi-\xi_\beta), \label{e:DVM_fail_test_fcn} \\
%     P_\beta(g) &=& \int_{\mathbb{R}}g(t,x,\xi) ~ \delta(\xi-\xi_\beta) d\xi = g(t,x,\xi_\beta). \label{e:DVM_fail_projection}
% \end{eqnarray}
% 
% \begin{equation}
%     \label{e:DVM_fail_expansion}
%     \widetilde{f}(t,x,\xi) = \sum_{\alpha=0}^n f_\alpha(t,x) \delta(\xi-\xi_\alpha).
% \end{equation}
% 
% Using the procedure of the previous chapter, we want to write down the PDE system for $\vect{u}$. As there is no continuous representation of $\widetilde{f}$ according to Equation \eqref{e:DVM_fail_expansion}, we cannot directly evaluate the derivative terms $\partial_{\xi} \widetilde{f}$ in Equation \eqref{e:boltzmann_transformed_with_ansatz} and consider finite difference approximations of the following kind:
