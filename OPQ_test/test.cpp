#include "../OPQ"
#include <iostream>

using namespace Eigen;
using namespace std;

class Legendre_test{
  public:
  long double eval(const  long double& x)
  {
    return 1;
  }
  const int a = -1;
  const int b = 1;
};

class Chebyshev_test{
public:
  long double eval(const long double& x)
  {
    return 1./sqrt(1.-x*x);
  }
  const int a = -1;
  const int b = 1;
};

int main(int argc, char* argv[]){
  
  OrthPolFormula<long double, Matrix<long double, Dynamic, 1> > OPF;
  
  int N_Coeff = atoi(argv[1]);
  
  Matrix<long double, Dynamic, 1> alpha(2*N_Coeff+1), beta(2*N_Coeff+1);
  
  //~ OPF.getLegendre(2*N_Coeff+1, alpha, beta);
  //~ OPF.getChebyshev(2*N_Coeff+1, alpha, beta);
  alpha.setZero();
  beta.setZero();
  
  
  cout.precision(15);
  //~ cout << "alphas: \n" << alpha << endl << "betas: \n" << beta << endl;
  
  Matrix<long double, Dynamic, Dynamic> abm(2, 2*N_Coeff+1);
  abm.row(0) = alpha.transpose();
  abm.row(1) = beta.transpose();
  
  
  Matrix<long double, Dynamic, Dynamic> result(2, 2*N_Coeff+1);
  Legendre_test wftest;
  //~ Chebyshev_test wftest;
  
  Chebyshev<long double, Matrix<long double, Dynamic, Dynamic>, Legendre_test>(abm, wftest, result, 100, N_Coeff);
  //~ Chebyshev<long double, Matrix<long double, Dynamic, Dynamic>, Chebyshev_test>(abm, wftest, result, 1, N_Coeff, 2);
  
  //~ cout << "\nAlphas generiert: \n" << result.row(0).transpose() << endl << "Betas generiert: \n" << result.row(1).transpose() << endl;
  
  
  OPF.getLegendre(2*N_Coeff+1, alpha, beta);
  
  
  
  Matrix<long double, Dynamic, Dynamic> Fehler(2, 2*N_Coeff+1);
  Fehler = result;
  Fehler.row(0) -= alpha;
  Fehler.row(1) -= beta;
  
  cout << "\n\nFehler: \n" << Fehler.block(0,0,2,N_Coeff+1).transpose() << endl;

  
  return 0;
}
